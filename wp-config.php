<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'latamsos_wp95');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i0uwhboikyji6vkwhz4ehjc1bthb1rym7wcjqwqnwoh1sdujuthkzytxyx0cdznq');
define('SECURE_AUTH_KEY',  's2wmh0yo09qsklljwbx1ku0jndqsztm6vtjnqlpl3ctuui3guz0xwllexriqo3z4');
define('LOGGED_IN_KEY',    'ycwbdxh5sic57g77ktxkwcexph8lgsyduphmewmptle2omktzxq1a9naspzjrjda');
define('NONCE_KEY',        'bopguxr1nznceiydma7fwek9ghajboqoxeqfxg8o7zw0mumj6fhyc7t5mcpeqei6');
define('AUTH_SALT',        'ecuu1nw8ajo0lbuuwylygrsyeulggjdq6bb5elgxf3kqazkmfyfqksyp8km8gbr5');
define('SECURE_AUTH_SALT', 'qi295ay4rf8vpfwgs43zoy4d6e1uluaet5azn3ikxdl83eykdjif9tnefbliqwda');
define('LOGGED_IN_SALT',   'mpcktodabkqyotjwcpsbvso0g9ootxhctkqtuoftyo6jxx0kfxyjsxhn5k6lzuj4');
define('NONCE_SALT',       'ufyiclj2qri4b4xdtevmfvbb8mb0ecatkgkww1ojq5wwe5klzzzeqkr2zgxm388g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpbx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
