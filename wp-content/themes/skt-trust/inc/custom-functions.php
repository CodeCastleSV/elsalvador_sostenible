<?php
/**
 * @package SKT Trust
 * Setup the WordPress core custom functions feature.
 *
*/

add_action('skt_trust_optionsframework_custom_scripts', 'skt_trust_optionsframework_custom_scripts');
function skt_trust_optionsframework_custom_scripts() { ?>
	<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('#example_showhidden').click(function() {
            jQuery('#section-example_text_hidden').fadeToggle(400);
        });
        if (jQuery('#example_showhidden:checked').val() !== undefined) {
            jQuery('#section-example_text_hidden').show();
        }
    });
    </script><?php
}


// get_the_content format text
function get_the_content_format( $str ){
	$raw_content = apply_filters( 'the_content', $str );
	$content = str_replace( ']]>', ']]&gt;', $raw_content );
	return $content;
}
// the_content format text
function the_content_format( $str ){
	echo get_the_content_format( $str );
}

function is_google_font( $font ){
	$notGoogleFont = array( 'Arial', 'Comic Sans MS', 'FreeSans', 'Georgia', 'Lucida Sans Unicode', 'Palatino Linotype', 'Symbol', 'Tahoma', 'Trebuchet MS', 'Verdana' );
	if( in_array($font, $notGoogleFont) ){
		return false;
	}else{
		return true;
	}
}

// subhead section function
function sub_head_section( $more ) {
	$pgs = 0;
	do {
		$pgs++;
	} while ($more > $pgs);
	return $pgs;
}

// remove excerpt more
function new_excerpt_more( $more ) {
	return '... ';
}
add_filter('excerpt_more', 'new_excerpt_more');

// get post categories function
function getPostCategories(){
	$categories = get_the_category();
	$catOut = '';
	$separator = ' / ';
	$catOutput = '';
	if($categories){
		foreach($categories as $category) {
			$catOutput .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", 'skt-trust' ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
		}
		$catOut = ''.trim($catOutput, $separator);
	}
	return $catOut;
}

// replace last occurance of a string.
function str_lreplace($search, $replace, $subject){
	$pos = strrpos($subject, $search);
	if($pos !== false){
		$subject = substr_replace($subject, $replace, $pos, strlen($search));
	}
	return $subject;
}

function readmore_func( $atts) {
	extract(shortcode_atts(array(	
	'button'	=> '',	
	'links'		=> '',
	'align'		=> '',						
	'target'	=> '',						
	), $atts));
	$rrow = '<div class="view-all-btn" style="text-align:'.$align.';"><a href="'.$links.'" target="'.$target.'">'.$button.'</a></div>';
    return $rrow;
}
add_shortcode( 'readmore-link', 'readmore_func' );


// custom post type for Testimonials
function my_custom_post_testimonials() {
	$labels = array(
		'name'               => __( 'Testimonials','skt-trust'),
		'singular_name'      => __( 'Testimonials','skt-trust'),
		'add_new'            => __( 'Add Testimonials','skt-trust'),
		'add_new_item'       => __( 'Add New Testimonial','skt-trust'),
		'edit_item'          => __( 'Edit Testimonial','skt-trust'),
		'new_item'           => __( 'New Testimonial','skt-trust'),
		'all_items'          => __( 'All Testimonials','skt-trust'),
		'view_item'          => __( 'View Testimonial','skt-trust'),
		'search_items'       => __( 'Search Testimonial','skt-trust'),
		'not_found'          => __( 'No Testimonial found','skt-trust'),
		'not_found_in_trash' => __( 'No Testimonial found in the Trash','skt-trust'), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Testimonials'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Manage Testimonials',
		'public'        => true,
		'menu_icon'		=> 'dashicons-format-quote',
		'menu_position' => null,
		'supports'      => array( 'title', 'editor', 'thumbnail'),
		'has_archive'   => true,
	);
	register_post_type( 'testimonials', $args );	
}
add_action( 'init', 'my_custom_post_testimonials' );

// add meta box to testimonials
add_action( 'admin_init', 'my_testimonial_admin_function' );
function my_testimonial_admin_function() {
    add_meta_box( 'testimonial_meta_box',
        'Testimonial Info',
        'display_testimonial_meta_box',
        'testimonials', 'normal', 'high'
    );
}
// add meta box form to testimonial
function display_testimonial_meta_box( $testimonial ) {
    // Retrieve current name of the Director and Movie Rating based on review ID
    $position = esc_html( get_post_meta( $testimonial->ID, 'position', true ) ); 
	
    ?>
    <table width="100%">       
        <tr>
            <td width="20%">Designation </td>
            <td width="80%"><input size="80" type="text" name="position" value="<?php echo $position; ?>" /></td>
        </tr>       
    </table>
    <?php    
}
// save testimonial meta box form data
add_action( 'save_post', 'add_testimonial_fields_function', 10, 2 );
function add_testimonial_fields_function( $testimonial_id, $testimonial ) {
    // Check post type for testimonials
    if ( $testimonial->post_type == 'testimonials' ) {
        // Store data in post meta table if present in post data		 
        if ( isset($_POST['position']) ) {
            update_post_meta( $testimonial_id, 'position', $_POST['position'] );
        }       
    }
}

//Testimonials function
function testimonials_output_func( $atts ){
	$testimonialoutput = '<div class="our-testimonials"><div class="owl-carousel">';
	wp_reset_query();
	query_posts('post_type=testimonials');
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
		$position = esc_html( get_post_meta( get_the_ID(), 'position', true ) );		
				$testimonialoutput .='<div class="testimonial-item">
								<div class="testimonial-content">
								<i class="fa fa-quote-left fa-2x"></i>
								<p>'.get_the_content().'</p>
								<div class="testimonial-thumb">'.get_the_post_thumbnail().'</div>
								<h5>'.get_the_title().'</h5>
								<div class="testimonial-detail">'.$position.'</div>
								</div>
							</div>';
		endwhile;
		 $testimonialoutput .= '</div>
    </div>';
	else:
	  $testimonialoutput = '<div class="our-testimonials">
	  						<div class="owl-carousel">
								<div class="testimonial-item">
									<div class="testimonial-content">
									<i class="fa fa-quote-left fa-2x"></i>
									<p>Fusce eu finibus ante, sit amet posuere massa. Cras ultricies, dolor in consectetur aliquam, felis dui porttitor ipsum, ut mollis erat leo vel massa. Maecenas consectetur eros et ligula hendrerit iaculis. Donec ut auctor neque, et pulvinar felis. Etiam blandit eros nec sodales euismod. Etiam facilisis dui in faucibus fermentum. Proin accumsan cursus ante a pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eleifend fringilla sapien</p>
									<div class="testimonial-thumb"><img src="'.get_template_directory_uri().'/images/testimonial-1.jpg" alt=""></div>
									<h5>John Doe</h5>
									<div class="testimonial-detail">(Director & CEO)</div>
									</div>
									</div>
									<div class="testimonial-item">
									<div class="testimonial-content">
									<i class="fa fa-quote-left fa-2x"></i>
									<p>Fusce eu finibus ante, sit amet posuere massa. Cras ultricies, dolor in consectetur aliquam, felis dui porttitor ipsum, ut mollis erat leo vel massa. Maecenas consectetur eros et ligula hendrerit iaculis. Donec ut auctor neque, et pulvinar felis. Etiam blandit eros nec sodales euismod. Etiam facilisis dui in faucibus fermentum. Proin accumsan cursus ante a pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus. Maecenas eleifend fringilla sapien</p>
									<div class="testimonial-thumb"><img src="'.get_template_directory_uri().'/images/testimonial-2.jpg" alt=""></div>
									<h5>John Doe</h5>
									<div class="testimonial-detail">(Director & CEO)</div>
									</div>
								</div>
								</div>
						  </div>
                        
    </div><a href="'.of_get_option('viewalltestimpnialurllink').'" class="read-more">'.of_get_option('viewalltestimonialbtntext').'</a>';			
	  endif;  
	wp_reset_query();
	return $testimonialoutput;
}
add_shortcode( 'testimonials', 'testimonials_output_func' );


// View All Testimonials function
function all_testimonials_output_func( $atts ){
	$count= 0;
	$testimonialoutput = '<div class="view-all-testimonials">';
	wp_reset_query();
	query_posts('post_type=testimonials&posts_per_page=-1');
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
		$count++;	
		if( $count%2==0 ) $nomgn = 'last'; else $nomgn = ' ';	
		$possition = esc_html( get_post_meta( get_the_ID(), 'possition', true ) );
				
				$testimonialoutput .='
				<div class="testimonial-col '.$nomgn.'">			   
 				   <div class="tm_description"><i class="fa fa-quote-left"></i>				   
				   		<p>'.get_the_content().'</p>
					</div>
					<div class="testimonial-thumb">'.get_the_post_thumbnail().'</div>
					<div class="testimonial-right"><h5>'.get_the_title().'</h5>
					<span>'.$position = esc_html( get_post_meta( get_the_ID(), 'position', true ) ).'</span>
					</div>
	             </div>';
				
		endwhile;
		 $testimonialoutput .= '</div>';
		 else :
		 $testimonialoutput .= '<h6>Sorry, no testimonial were found</h6>';
	  endif;  
	wp_reset_query();
	
	return $testimonialoutput;
}
add_shortcode( 'view_all_testimonials', 'all_testimonials_output_func' );

/*Latest news Function*/
function footer_recent_posts_func( $atts ){
   extract( shortcode_atts( array(
		'show' => 4,
	), $atts ) );
	$frp = '';
	wp_reset_query();
	$n = 0;
	query_posts(  array( 'posts_per_page'=>$show, 'post__not_in' => get_option('sticky_posts') )  );
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			$n++;
			if( $n%3==0 ) $nomgn = 'last'; else $nomgn = ' ';
			
			
			$frp .= '<div class="recent-post '.$nomgn.'">
							 <a href="'.get_the_permalink().'"><h6>'.get_the_title().'</h6></a>
							 <p>'.wp_trim_words(get_the_content(), of_get_option('footerpostconlength')).'</p>
							<div class="clear"></div>
                        </div>';		
		endwhile;
	endif;
	wp_reset_query();

	return $frp;
}
add_shortcode( 'footer-posts', 'footer_recent_posts_func' );

//custom post type for Our Team
function my_custom_post_staff() {
	$labels = array(
		'name'               => __( 'Our Team', 'skt-trust' ),
		'singular_name'      => __( 'Our Team', 'skt-trust' ),
		'add_new'            => __( 'Add New', 'skt-trust' ),
		'add_new_item'       => __( 'Add New Team Member', 'skt-trust' ),
		'edit_item'          => __( 'Edit Team Member', 'skt-trust' ),
		'new_item'           => __( 'New Member', 'skt-trust' ),
		'all_items'          => __( 'All Members', 'skt-trust' ),
		'view_item'          => __( 'View Members', 'skt-trust' ),
		'search_items'       => __( 'Search Team Members', 'skt-trust' ),
		'not_found'          => __( 'No Team members found', 'skt-trust' ),
		'not_found_in_trash' => __( 'No Team members found in the Trash', 'skt-trust' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Our Team'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Manage Team',
		'public'        => true,
		'menu_position' => null,
		'menu_icon'		=> 'dashicons-groups',
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'rewrite' => array('slug' => 'our-team'),
		'has_archive'   => true,
	);
	register_post_type( 'team', $args );
}
add_action( 'init', 'my_custom_post_staff' );
// add meta box to Team
add_action( 'admin_init', 'my_team_admin_function' );
function my_team_admin_function() {
    add_meta_box( 'team_meta_box',
        'Member Info',
        'display_team_meta_box',
        'team', 'normal', 'high'
    );
}
// add meta box form to team
function display_team_meta_box( $team ) {
    // Retrieve current name of the Director and Movie Rating based on review ID
    $designation = esc_html( get_post_meta( $team->ID, 'designation', true ) );
    $facebook = get_post_meta( $team->ID, 'facebook', true );
	$facebooklink = esc_url( get_post_meta( $team->ID, 'facebooklink', true ) );
    $twitter = get_post_meta( $team->ID, 'twitter', true );
	$twitterlink = esc_url( get_post_meta( $team->ID, 'twitterlink', true ) );
    $linkedin = get_post_meta( $team->ID, 'linkedin', true );
	$linkedinlink = esc_url( get_post_meta( $team->ID, 'linkedinlink', true ) );
	$pint = get_post_meta( $team->ID, 'google', true );
	$googlelink = esc_url( get_post_meta( $team->ID, 'googlelink', true ) );
    $dribbble = get_post_meta( $team->ID, 'dribbble', true );
	$dribbblelink = get_post_meta( $team->ID, 'dribbblelink', true );
    ?>
    <table width="100%">
        <tr>
            <td width="20%">Designation </td>
            <td width="80%"><input type="text" name="designation" value="<?php echo $designation; ?>" /></td>
        </tr>
        <tr>
            <td width="20%">Social link 1</td>
            <td width="40%"><input type="text" name="facebook" value="<?php echo $facebook; ?>" /></td>
            <td width="40%"><input style="width:500px;" type="text" name="facebooklink" value="<?php echo $facebooklink; ?>" /></td>
        </tr>
        <tr>
            <td width="20%">Social Link 2</td>
            <td width="40%"><input type="text" name="twitter" value="<?php echo $twitter; ?>" /></td>
            <td width="40%"><input style="width:500px;" type="text" name="twitterlink" value="<?php echo $twitterlink; ?>" /></td>
        </tr>
        <tr>
            <td width="20%">Social Link 3</td>
            <td width="40%"><input type="text" name="linkedin" value="<?php echo $linkedin; ?>" /></td>
            <td width="40%"><input style="width:500px;" type="text" name="linkedinlink" value="<?php echo $linkedinlink; ?>" /></td>
        </tr>
        <tr>
            <td width="20%">Social Link 4</td>
            <td width="40%"><input type="text" name="dribbble" value="<?php echo $dribbble; ?>" /></td>
            <td width="40%"><input style="width:500px;" type="text" name="dribbblelink" value="<?php echo $dribbblelink; ?>" /></td>
        </tr>
        <tr>
        	<td width="100%" colspan="3"><label style="font-size:15px;"><strong>Note: Icon name should be in lowercase without space.</strong></label></td>
        </tr>
    </table>
    <?php
}
// save team meta box form data
add_action( 'save_post', 'add_team_fields_function', 10, 2 );
function add_team_fields_function( $team_id, $team ) {
    // Check post type for team
    if ( $team->post_type == 'team' ) {
        // Store data in post meta table if present in post data
        if ( isset($_POST['designation']) ) {
            update_post_meta( $team_id, 'designation', $_POST['designation'] );
        }
        if ( isset($_POST['facebook']) ) {
            update_post_meta( $team_id, 'facebook', $_POST['facebook'] );
        }
		if ( isset($_POST['facebooklink']) ) {
            update_post_meta( $team_id, 'facebooklink', $_POST['facebooklink'] );
        }
        if ( isset($_POST['twitter']) ) {
            update_post_meta( $team_id, 'twitter', $_POST['twitter'] );
        }
		if ( isset($_POST['twitterlink']) ) {
            update_post_meta( $team_id, 'twitterlink', $_POST['twitterlink'] );
        }
        if ( isset($_POST['linkedin']) ) {
            update_post_meta( $team_id, 'linkedin', $_POST['linkedin'] );
        }
		if ( isset($_POST['linkedinlink']) ) {
            update_post_meta( $team_id, 'linkedinlink', $_POST['linkedinlink'] );
        }
        if ( isset($_POST['dribbble']) ) {
            update_post_meta( $team_id, 'dribbble', $_POST['dribbble'] );
        }
		if ( isset($_POST['dribbblelink']) ) {
            update_post_meta( $team_id, 'dribbblelink', $_POST['dribbblelink'] );
        }
		if ( isset($_POST['google']) ) {
            update_post_meta( $team_id, 'google', $_POST['google'] );
        }
		if ( isset($_POST['googlelink']) ) {
            update_post_meta( $team_id, 'googlelink', $_POST['googlelink'] );
        }
    }
}

function ourteamposts_func( $atts ) {
   extract( shortcode_atts( array(
		'col' => '',
	), $atts ) );
	  extract( shortcode_atts( array( 'show' => '',), $atts ) ); $postoutput = ''; wp_reset_query();
	$bposts = '<div class="section-teammember">';
	$args = array( 'post_type' => 'team', 'posts_per_page' => $show, 'post__not_in' => get_option('sticky_posts'), 'orderby' => 'date', 'order' => 'desc' );
	query_posts( $args );
	$n = 0;
	if ( have_posts() ) {
		while ( have_posts() ) { 
			the_post();
			$n++; if( $n%4 == 0 ) $nomargn = ' last'; else $nomargn = '';
			$designation = esc_html( get_post_meta( get_the_ID(), 'designation', true ) );
			$facebook = get_post_meta( get_the_ID(), 'facebook', true );
			$facebooklink = get_post_meta( get_the_ID(), 'facebooklink', true );
			$twitter = get_post_meta( get_the_ID(), 'twitter', true );
			$twitterlink = get_post_meta( get_the_ID(), 'twitterlink', true );
			$linkedin = get_post_meta( get_the_ID(), 'linkedin', true );
			$linkedinlink = get_post_meta( get_the_ID(), 'linkedinlink', true );
			$dribbble = get_post_meta( get_the_ID(), 'dribbble', true );
			$dribbblelink = get_post_meta( get_the_ID(), 'dribbblelink', true );
			$pint = get_post_meta( get_the_ID(), 'google', true );
			$googlelink = get_post_meta( get_the_ID(), 'googlelink', true );
			$bposts .= '<div class="ourteam_col '.$nomargn.'">';	
			$bposts .= '<div class="ourteam_thumb"><a href="'.get_permalink().'" title="'.get_the_title().'">'.( (get_the_post_thumbnail( get_the_ID(), 'thumbnail') != '') ? get_the_post_thumbnail( get_the_ID(), array( 270, 310 )) : '<img src="'.get_template_directory_uri().'/images/img_404.png" width="270" height="auto" />' ).'</a></div>';
		
			$bposts .= '<div class="ourteam_content">';
			$bposts .= '<a href="'.get_the_permalink() . '"><h5>'.get_the_title().'</h5></a>';
			$bposts .= '<h6>'.$designation.'</h6>';
			$bposts .= '<p>'.wp_trim_words(get_the_content(), of_get_option('contentlenghtourteam')).'</p>';
			$bposts .= '</div>';
			$bposts .= '<div class="social-icons">';
			if( $facebook != '' ){
				$bposts .= '<a href="'.$facebooklink.'" title="'.$facebook.'" target="_blank"><i class="fa fa-'.$facebook.'"></i></a>';
			}
			if( $twitter != '' ){
				$bposts .= '<a href="'.$twitterlink.'" title="'.$twitter.'" target="_blank"><i class="fa fa-'.$twitter.'"></i></a>';
			}
			if( $linkedin != '' ){
				$bposts .= '<a href="'.$linkedinlink.'" title="'.$linkedin.'" target="_blank"><i class="fa fa-'.$linkedin.'"></i></a>';
			}
			if( $dribbble != '' ){
				$bposts .= '<a href="'.$dribbblelink.'" title="'.$dribbble.'" target="_blank"><i class="fa fa-'.$dribbble.'"></i></a>';
			}
			if( $pint != '' ){
				$bposts .= '<a href="'.$googlelink.'" title="'.$pint.'" target="_blank"><i class="fa fa-'.$pint.'"></i></a>';
			}
			$bposts .= '<div class="clear"></div></div></div>';
 		}
	}else{
		$bposts .= '
		<div class="ourteam_col">
				<div class="ourteam_thumb"><a href="#"><img src="'.get_template_directory_uri().'/images/ourteam-1.jpg"></a></div>
				
			<div class="ourteam_content">
				<a href="#"><h5>David Doe</h5></a>
				<h6>(Founder & Ceo)</h6>
				<p>Quisque posuere odio exscommodo purus rhoncus non. Integer faucibus lorem et purus lacinia at ullamcorper nisl rhoncus. Mauris quis sequis elit</p>
			</div>
			<div class="social-icons">
					<a target="_blank" title="facebook" href="#"><i class="fa fa-facebook"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-twitter"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-linkedin"></i></a>
					<div class="clear"></div>
				</div>
		</div>

		<div class="ourteam_col">
				<div class="ourteam_thumb"><a href="#"><img src="'.get_template_directory_uri().'/images/ourteam-2.jpg"></a></div>
				
			<div class="ourteam_content">
				<a href="#"><h5>John Doe</h5></a>
				<h6>(Business Analyst)</h6>
				<p>Quisque posuere odio exscommodo purus rhoncus non. Integer faucibus lorem et purus lacinia at ullamcorper nisl rhoncus. Mauris quis sequis elit</a></p>
			</div>
			<div class="social-icons">
					<a target="_blank" title="facebook" href="#"><i class="fa fa-facebook"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-twitter"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-linkedin"></i></a>
					<div class="clear"></div>
				</div>
		</div>

		<div class="ourteam_col">
				<div class="ourteam_thumb"><a href="#"><img src="'.get_template_directory_uri().'/images/ourteam-3.jpg"></a></div>
				
			<div class="ourteam_content">
				<a href="#"><h5>Martina Doe</h5></a>
				<h6>(Designer)</h6>
				<p>Quisque posuere odio exscommodo purus rhoncus non. Integer faucibus lorem et purus lacinia at ullamcorper nisl rhoncus. Mauris quis sequis elit</a></p>
			</div>
			<div class="social-icons">
					<a target="_blank" title="facebook" href="#"><i class="fa fa-facebook"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-twitter"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-linkedin"></i></a>
					<div class="clear"></div>
				</div>
		</div>

		<div class="ourteam_col last">
				<div class="ourteam_thumb"><a href="#"><img src="'.get_template_directory_uri().'/images/ourteam-4.jpg"></a></div>
				
			<div class="ourteam_content">
				<a href="#"><h5>Joseph Doe</h5></a>
				<h6>(Marketing Head)</h6>
				<p>Quisque posuere odio exscommodo purus rhoncus non. Integer faucibus lorem et purus lacinia at ullamcorper nisl rhoncus. Mauris quis sequis elit</a></p>
			</div>
			<div class="social-icons">
					<a target="_blank" title="facebook" href="#"><i class="fa fa-facebook"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-twitter"></i></a>
					<a target="_blank" title="twitter" href="#"><i class="fa fa-linkedin"></i></a>
					<div class="clear"></div>
				</div>
		</div>
		
		
		<div class="clear"></div>';
	}
	wp_reset_query();
	$bposts .= '<div class="clear"></div></div>';
    return $bposts;
}
add_shortcode( 'ourteam', 'ourteamposts_func' );

// shortcode for custom width
function skt_trust_custom_width_func($atts, $content = null){
		extract(shortcode_atts(array(
			'width'		=> 'width',						
			'class'		=> null
		), $atts));
		return '<div class="'.$class.'" style="width:'.$width.';">'.do_shortcode($content).'</div>';		
}
add_shortcode('area','skt_trust_custom_width_func');


// Social Icon Shortcodes
function skt_trust_social_area($atts,$content = null){
  return '<div class="social-icons">'.do_shortcode($content).'</div>';
 }
add_shortcode('social_area','skt_trust_social_area');

function skt_trust_social($atts){
 extract(shortcode_atts(array(
  'icon' => '',
  'link' => ''
 ),$atts));
  return '<a href="'.$link.'" target="_blank" class="fa fa-'.$icon.'" title="'.$icon.'"></a>';
 }
add_shortcode('social','skt_trust_social');

//contact form
function contactform_func( $atts ) {
    $atts = shortcode_atts( array(
        'to_email' => get_bloginfo('admin_email'),
		'title' => 'Contact enquiry - '.home_url( '/' ),
    ), $atts );

	$cform = "<div class=\"main-form-area\" id=\"contactform_main\">";

	$cerr = array();
	if( isset($_POST['c_submit']) && $_POST['c_submit']=='Submit' ){
		$name 			= trim( $_POST['c_name'] );
		$lastname		= trim( $_POST['c_lastname'] );
		$email 			= trim( $_POST['c_email'] ) ;
		$phone 			= trim( $_POST['c_phone'] );
		$comments 		= trim( $_POST['c_comments'] );

		if( !$name )
			$cerr['name'] = 'Please enter your first name.';
		if( !$lastname )
			$cerr['lastname'] = 'Please enter your last name.';
		if( ! filter_var($email, FILTER_VALIDATE_EMAIL) ) 
			$cerr['email'] = 'Please enter a valid email.';
		if( !$phone )
			$cerr['phone'] = 'Please enter your phone number.';
		if( !$comments )
			$cerr['comments'] = 'Please enter your message.';

		if( count($cerr) == 0 ){
			$subject = $atts['title'];
			$headers = "From: ".$name." <" . strip_tags($email) . ">\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			$message = '<html><body>
							<table>
								<tr><td>Name: </td><td>'.$name.'</td></tr>
								<tr><td>Email: </td><td>'.$email.'</td></tr>
								<tr><td>Phone: </td><td>'.$phone.'</td></tr>
								<tr><td>Last Name: </td><td>'.$lastname.'</td></tr>
								<tr><td>Message: </td><td>'.$comments.'</td></tr>
							</table>
						</body>
					</html>';
			mail( $atts['to_email'], $subject, $message, $headers);
			$cform .= '<div class="success_msg">Thank you! A representative will get back to you very shortly.</div>';
			unset( $name, $lastname, $email, $phone, $comments );
		}else{
			$cform .= '<div class="error_msg">';
			$cform .= implode('<br />',$cerr);
			$cform .= '</div>';
		}
	}
	$capNum1 	= rand(1,4);
	$capNum2 	= rand(1,5);
	$capSum		= $capNum1 + $capNum2;
	$sumStr		= $capNum1." + ".$capNum2 ." = ";

	$cform .= "<form name=\"contactform\" action=\"#contactform_main\" method=\"post\">
			<input type=\"text\" name=\"c_name\" class=\"namefield\"  value=\"". ( ( empty($name) == false ) ? $name : "" ) ."\" placeholder=\"First Name\" />
			<input type=\"text\" name=\"c_lastname\" class=\"webfield\" value=\"". ( ( empty($lastname) == false ) ? $lastname : "" ) ."\" placeholder=\"Last Name\" />
			<input type=\"email\" name=\"c_email\" class=\"emailfield\" value=\"". ( ( empty($email) == false ) ? $email : "" ) ."\" placeholder=\"Email Address\" />
			<input type=\"tel\" name=\"c_phone\" class=\"phonefield\" value=\"". ( ( empty($phone) == false ) ? $phone : "" ) ."\" placeholder=\"Phone No\" /><div class=\"clear\"></div>
			
			<textarea name=\"c_comments\" class=\"messagefield\" placeholder=\"Message\">". ( ( empty($comments) == false ) ? $comments : "" ) ."</textarea><div class=\"clear\"></div>";
	$cform .= "<p class=\"sub\"><input type=\"submit\" name=\"c_submit\" value=\"Submit\" class=\"search-submit\" /></p>
		</form>
	</div>";
    return $cform;
}
add_shortcode( 'contactform', 'contactform_func' );

//custom post type for Our photogallery
function my_custom_post_photogallery() {
	$labels = array(
		'name'               => __( 'Photo Gallery','skt-restaurant' ),
		'singular_name'      => __( 'Photo Gallery','skt-restaurant' ),
		'add_new'            => __( 'Add New','skt-restaurant' ),
		'add_new_item'       => __( 'Add New Image','skt-restaurant' ),
		'edit_item'          => __( 'Edit Image','skt-restaurant' ),
		'new_item'           => __( 'New Image','skt-restaurant' ),
		'all_items'          => __( 'All Images','skt-restaurant' ),
		'view_item'          => __( 'View Image','skt-restaurant' ),
		'search_items'       => __( 'Search Images','skt-restaurant' ),
		'not_found'          => __( 'No images found','skt-restaurant' ),
		'not_found_in_trash' => __( 'No images found in the Trash','skt-restaurant' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'Photo Gallery'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Manage Photo Gallery',
		'public'        => true,
		'menu_icon'		=> 'dashicons-format-image',
		'menu_position' => null,
		'supports'      => array( 'title', 'thumbnail' ),
		'has_archive'   => true,
	);
	register_post_type( 'photogallery', $args );
}
add_action( 'init', 'my_custom_post_photogallery' );


//  register gallery taxonomy
register_taxonomy( "gallerycategory", 
	array("photogallery"), 
	array(
		"hierarchical" => true, 
		"label" => "Gallery Category", 
		"singular_label" => "Photo Gallery", 
		"rewrite" => true
	)
);

add_action("manage_posts_custom_column",  "photogallery_custom_columns");
add_filter("manage_edit-photogallery_columns", "photogallery_edit_columns");
function photogallery_edit_columns($columns){
	$columns = array(
		"cb" => '<input type="checkbox" />',
		"title" => "Gallery Title",
		"pcategory" => "Gallery Category",
		"view" => "Image",
		"date" => "Date",
	);
	return $columns;
}
function photogallery_custom_columns($column){
	global $post;
	switch ($column) {
		case "pcategory":
			echo get_the_term_list($post->ID, 'gallerycategory', '', ', ','');
		break;
		case "view":
			the_post_thumbnail('thumbnail');
		break;
		case "date":

		break;
	}
}

//[photogallery filter="false"]
function photogallery_shortcode_func( $atts ) {
	extract( shortcode_atts( array(
		'show' => -1,
		'filter' => 'true'
	), $atts ) );
	$pfStr = '';
	
	if( $filter == 'true' ){
	$pfStr .= '<div class="controls"><ul><li class="filter" data-filter="all">'.esc_html__(of_get_option('showalltextfilgr')).'</li>';
	$categories = get_categories( array('taxonomy' => 'gallerycategory') );
	foreach ($categories as $category) {
		$pfStr .= '<li class="filter" data-filter="'.$category->term_id.'">'.$category->name.'</li>';
	}
	$pfStr .= '</ul></div>';
	}

	$pfStr .= '<div class="gallery"><ul id="Grid">';
	$j=0;
	$args = array(
			   'post_type'  => 'photogallery',
			   'posts_per_page' => $show,
				'meta_query' => array(
					array(
					 'key' => '_thumbnail_id',
					 'compare' => 'EXISTS'
					),
				)
			);
	query_posts($args); 
	if ( have_posts() ) : 
	while ( have_posts() ) : the_post(); 
	$j++;	
	$imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
	$terms = wp_get_post_terms( get_the_ID(), 'gallerycategory', array("fields" => "all"));
	$slugAr = array();
	foreach( $terms as $tv ){
		$slugAr[] = $tv->term_id;
	}
	$pfStr .= '<li class="mix '.implode(' ', $slugAr).'" '.( ($j%4==0) ? 'style="margin-right:0"' : '' ).'>
			   <a href="'.$imgSrc[0].'" rel="prettyPhoto[pp_gal]"><img src="'.$imgSrc[0].'"/></a></li>';
	unset( $slugAr );
	endwhile; else: 
		$pfStr .= '<p>Sorry, photo gallery is empty.</p>';
	endif; 
	wp_reset_query();	
	$pfStr .= '</ul></div>';
	return $pfStr;
}
add_shortcode( 'photogallery', 'photogallery_shortcode_func' );

function heading_title_func( $atts, $content = null ) {
   extract( shortcode_atts( array(	
	'underline'	=> '',
	'align'		=> ''	
	), $atts ) ); 
	$hdntitle = '<h2 class="heading '.( (strtolower($underline) == 'yes') ? 'underline' : '' ).'" '.( ($align!='') ? 'style="text-align:'.$align.' !important;"' : '' ) .'>'.do_shortcode( $content ) .'</h2>'; 
    return $hdntitle;
}
add_shortcode( 'heading', 'heading_title_func' );
 
 
// Latest Post
function latestpostsoutput_func( $atts ){
   extract( shortcode_atts( array(
		'show' => '',
	), $atts ) );
	$postoutput = '';
	$postoutput .= '<div class="latestnews">';
	wp_reset_query();
	$n = 0;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	query_posts(  array( 'post_type'=> 'post', 'posts_per_page'=>$show, 'post__not_in' => get_option('sticky_posts'), 'paged' => $paged )  );
	if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			$n++;
			if( $n%4==0 )  $nomgn = ' last';	else $nomgn = '';
			if ( has_post_thumbnail()) {
				$large_imgSrc = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large');
				$imgUrl = $large_imgSrc[0];
			}else{
				$imgUrl = get_template_directory_uri().'/images/img_404.png';
			}
			$postoutput .= '
			<div class="news-box'.$nomgn.'">
				<div class="news-thumb">
					<a href="'.get_the_permalink().'"><img src="'.$imgUrl.'" alt="" /></a>
				</div>
				<div class="news">
					<a href="'.get_the_permalink().'"><h5>'.get_the_title().'</h5></a>
					<span class="admin-post"><i class="fa fa-user"></i> '.esc_html__('By','skt-trust').' <a href="'.get_author_posts_url( get_the_author_meta( "ID" ), get_the_author_meta( 'user_nicename' ) ).'">'.get_the_author().'</a></span>
					<span class="post-date"><i class="fa fa-calendar"></i> '.esc_html__('on ','skt-trust'). get_the_date('d F Y').'</span>

					<p>'.wp_trim_words( get_the_content(), of_get_option('newscontentlength'), '...' ).'</p>
					<a href="'.get_the_permalink().'" class="read-more">'.of_get_option('readmoretext').'</a>
				</div>
			</div>';	
			
		endwhile;
	endif;
	$postoutput .= '<div class="clear"></div>';
	
	$postoutput .= '</div>';
	wp_reset_query();
	return $postoutput;
}
add_shortcode( 'latestposts', 'latestpostsoutput_func' );

//Accordion Code
function column_content_func_row($atts, $content = null){
	return '<div class="cloumn_row">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('column_row','column_content_func_row');

//[column_content]Your content here...[/column_content]
function column_content_func( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'class' => '',
		'background' => '',
		'type' => '',
	), $atts ) );
	$colPos = strpos($type, '_last');
	if($colPos === false){
		$cnt = '<div class="'.$type.'" class="'.$class.'" style="background:'.$background.'">'.do_shortcode($content).'</div>';
	}else{
		$type = substr($type,0,$colPos);
		$cnt = '<div class="'.$type.' last_column" class="'.$class.'" style="background:'.$background.'">'.do_shortcode($content).'</div>';
	}
	return $cnt;
}
add_shortcode( 'column_content', 'column_content_func' );

function skt_trust_clear($atts, $content = null ){
	extract(shortcode_atts(array(
	'class'	=> '',
	), $atts));
	return '<div class="clear"></div>';
	}
add_shortcode('clear','skt_trust_clear');

//Accordion Code
function skt_trust_accordion_main($atts, $content = null){
	return '<div class="accordion-section"><div id="accordion">'.do_shortcode($content).'</div></div>';
	}
add_shortcode('accordion_section','skt_trust_accordion_main');

function skt_trust_accordion($atts, $content = null ){
	extract(shortcode_atts(array(
	'title'	=> '',
	'content'	=> '',
	), $atts));
	return '<h6>'.$title.'</h6><div><p>'.$content.'</p></div>';
	}
add_shortcode('accordion','skt_trust_accordion');

//skillbar Code
function skt_trust_skillbar_main($atts, $content = null){
	return '<div class="skillbar-section">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('skillbar_section','skt_trust_skillbar_main');

function skt_trust_skillbar($atts, $content = null ){
	extract(shortcode_atts(array(
	'title'	=> '',
	'title_color'	=> '',
	'content'	=> '',
	'content_color'	=> '',
	'skillbar_bgcolor'	=> '',
	'percent'	=> '',
	'percent_color'	=> '',
	'percent_bgcolor'	=> '',
	), $atts));
	return '<div class="skillbar-title" style="color:'.$title_color.';">'.$title.'</div>
	<div class="skillbar " data-percent="'.$percent.'" style="background:'.$skillbar_bgcolor.';">
	<div class="skillbar-bar" style="color:'.$content_color.'; background:'.$percent_bgcolor.';"><span>'.$content.'</span></div>
	<div class="skill-bar-percent" style="color:'.$percent_color.';">'.$percent.'</div>
</div>';
	}
add_shortcode('skillbar','skt_trust_skillbar');

//Causes Code
function skt_trust_causes_main($atts, $content = null){
	return '<div class="causes-section">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('causes_main','skt_trust_causes_main');
function skt_trust_causes($atts, $content = null ){
	extract(shortcode_atts(array(
	'image'	=> '',
	'title'	=> '',
	'content'	=> '',
	'skillbar_bgcolor'	=> '',
	'percent'	=> '',
	'percent_color'	=> '',
	'percent_bgcolor'	=> '',
	'raised_title'	=> '',
	'goal_title'	=> '',
	'raised_price'	=> '',
	'goal_price'	=> '',
	'donate_button'	=> '',
	'link'	=> '',
	'class'	=> '',
	), $atts));
	return '<div class="causes-col '.$class.'">
	<div class="causes-thumb"><img src="'.$image.'" alt="" /><a href="'.$link.'" class="read-more">'.$donate_button.'</a></div>
	<div class="causes-content"><h5>'.$title.'</h5>
	<p>'.$content.'</p>
	
	<div class="skillbar " data-percent="'.$percent.'" style="background:'.$skillbar_bgcolor.';">
	<div class="skillbar-bar" style="background:'.$percent_bgcolor.';">
	<div class="skill-bar-percent" style="color:'.$percent_color.'; background:'.$percent_bgcolor.';">'.$percent.'</div></div>
</div>
	<div class="cuase-raised"><h5><span>'.$raised_title.'</span>'.$raised_price.'</h5></div>
	<div class="cuase-goal"><h5><span>'.$goal_title.'</span>'.$goal_price.'</h5></div>
	<div class="clear"></div>
	</div>
</div>';
	}
add_shortcode('our_causes','skt_trust_causes');

//Choose Code
function skt_trust_choose_main($atts, $content = null){
	return '<div class="choose_us_main">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('choose_us_main','skt_trust_choose_main');

function skt_trust_choose_us($atts, $content = null ){
	extract(shortcode_atts(array(
	'title'	=> '',
	'icon'	=> '',
	'image'	=> '',
	'content'	=> '',
	'link'	=> '',
	), $atts));
	$output = '<div class="choose-col">';
	if($link){$output .= '<a href="'.$link.'">'; }
	if($icon){$output .= '<div class="choose-thumb"><i class="fa fa-'.$icon.' fa-4x"></i></div>';}
	if($image){$output .= '<div class="choose-thumb"><img src="'.$image.'" alt=""></div>';}
	$output .= '<div class="choose-content">';
	$output .= '<h5>'.$title.'</h5>';
	$output .= '<p>'.$content.'</p>';
	$output .= '</div>';
	if($link)$output .= '</a>';
	
	$output .= '</div>';
	return $output;
	}
add_shortcode('Choose_us','skt_trust_choose_us');

//Events Shortcode Code
function skt_trust_event_main($atts, $content = null){
	return '<div class="event_main">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('event_main','skt_trust_event_main');

function skt_trust_event($atts, $content = null ){
	extract(shortcode_atts(array(
	'image'	=> '',
	'event_date'	=> '',
	'event_month'	=> '',
	'title'	=> '',
	'event_time'	=> '',
	'event_time_icon'	=> '',
	'event_location_icon'	=> '',
	'event_location'	=> '',
	'content'	=> '',
	'button'	=> '',
	'link'	=> '',
	'class'	=> '',
	), $atts));
	return '<div class="event-col '.$class.'">
	<div class="event-thumb"><img src="'.$image.'" alt=""><h3><span>'.$event_date.'</span>'.$event_month.'</h3></div>
	<div class="event-content">
	<h3>'.$title.'</h3>
	<div class="event-time"><i class="fa fa-'.$event_time_icon.'"></i> '.$event_time.'</div>
	<div class="event-location"><i class="fa fa-'.$event_location_icon.'"></i> '.$event_location.'</div>
	<div class="clear"></div>
	<p>'.$content.'</p>
	<a href="'.$link.'" class="read-more">'.$button.'</a>
	</div>
	</div>';
	}
add_shortcode('event','skt_trust_event');

//Client Shortcode Code
function skt_trust_logo_main($atts, $content = null){
	return '<div class="client_logo_section">'.do_shortcode($content).'<div class="clear"></div></div>';
	}
add_shortcode('client_logo_section','skt_trust_logo_main');
// Client Logo
function skt_trust_client_logo($atts, $content = null ){
	extract(shortcode_atts(array(
	'image'	=> '',
	'link'	=> '',
	'class'	=> '',
	), $atts));
	return '<div class="client-logo '.$class.'"><a href="'.$link.'"><img src="'.$image.'" alt=""></a></div>';}
add_shortcode('client_logo','skt_trust_client_logo');

define('SKT_THEME_DOC','http://sktthemesdemo.net/documentation/skt-trust-doc/');
