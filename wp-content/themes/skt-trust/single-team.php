<?php
/**
 * The Template for displaying all single posts.
 *
 * @package SKT Trust
 */
get_header(); 
 
?>
<div class="content-area">
    <div class="middle-align">
        <div class="site-main sitefull">
        		<?php
                    $designation = esc_html( get_post_meta( get_the_ID(), 'designation', true ) );
                    $facebook = get_post_meta( get_the_ID(), 'facebook', true );
                    $facebooklink = get_post_meta( get_the_ID(), 'facebooklink', true );
                    $twitter = get_post_meta( get_the_ID(), 'twitter', true );
                    $twitterlink = get_post_meta( get_the_ID(), 'twitterlink', true );
                    $linkedin = get_post_meta( get_the_ID(), 'linkedin', true );
                    $linkedinlink = get_post_meta( get_the_ID(), 'linkedinlink', true );
                    $dribbble = get_post_meta( get_the_ID(), 'dribbble', true );
                    $dribbblelink = get_post_meta( get_the_ID(), 'dribbblelink', true );
                    $pint = get_post_meta( get_the_ID(), 'google', true );
                    $googlelink = get_post_meta( get_the_ID(), 'googlelink', true );
                ?>
			<?php while ( have_posts() ) : the_post(); ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <h5 class="member-desination"><?php echo $designation; ?></h5>
				<div class="social-icons">
                 <?php 	if( $facebook != '' ){
                        echo '<a href="'.$facebooklink.'" title="'.$facebook.'" target="_blank"><i class="fa fa-'.$facebook.'"></i></a>';                    }
                    if( $twitter != '' ){
                        echo '<a href="'.$twitterlink.'" title="'.$twitter.'" target="_blank"><i class="fa fa-'.$twitter.'"></i></a>';
                    }
                    if( $linkedin != '' ){
                        echo '<a href="'.$linkedinlink.'" title="'.$linkedin.'" target="_blank"><i class="fa fa-'.$linkedin.'"></i></a>';
                    }
                    if( $dribbble != '' ){
                        echo '<a href="'.$dribbblelink.'" title="'.$dribbble.'" target="_blank"><i class="fa fa-'.$dribbble.'"></i></a>';
                    }
                    if( $pint != '' ){
                        echo '<a href="'.$googlelink.'" title="'.$pint.'" target="_blank"><i class="fa fa-'.$pint.'"></i></a>';
                    } ?>
                </div>
                <div class="clear"></div>
                <?php the_post_thumbnail('medium', array('class' => 'alignleft') ); ?>
                <?php the_content(); ?>
                
			<?php endwhile; // end of the loop. ?>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php get_footer(); ?>