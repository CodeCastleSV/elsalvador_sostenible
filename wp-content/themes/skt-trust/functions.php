<?php
/**
 * SKT Trust functions and definitions
 *
 * @package SKT Trust
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
function content($limit) {
$content = explode(' ', get_the_content(), $limit);
if (count($content)>=$limit) {
array_pop($content);
$content = implode(" ",$content).'...';
} else {
$content = implode(" ",$content);
}	
$content = preg_replace('/\[.+\]/','', $content);
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]&gt;', $content);
return $content;
}

if ( ! function_exists( 'skt_trust_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function skt_trust_setup() {

	if ( ! isset( $content_width ) )
		$content_width = 640; /* pixels */
	load_theme_textdomain( 'skt-trust', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'woocommerce' );
	add_filter('widget_text', 'do_shortcode');
	add_theme_support( 'title-tag' );
	add_image_size('homepage-thumb',240,145,true);	
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'skt-trust' ),
		'footer' => __( 'Footer Menu', 'skt-trust' ),
	) );
	add_theme_support( 'custom-background', array(
	'default-color' => 'ffffff',
	'default-image' => '',
//	'default-image' => get_template_directory_uri().'/images/body-bg.jpg',

	) );
	add_editor_style( 'editor-style.css' );
}
endif; // skt_trust_setup
add_action( 'after_setup_theme', 'skt_trust_setup' );

function skt_trust_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'skt-trust' ),
		'description'   => __( 'Appears on blog page sidebar', 'skt-trust' ),
		'id'            => 'sidebar-1',
		'before_widget' => '',		
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
	) );

	register_sidebar( array(
		'name'          => __( 'Header Top Left Widget', 'skt-trust' ),
		'description'   => __( 'Appears on header top left area', 'skt-trust' ),
		'id'            => 'header-left-widget',
		'before_widget' => '<div class="header-top-left">',		
		'before_title'  => '<h3 style="display:none;">',
		'after_title'   => '</h3>',
		'after_widget'  => '</div>',
	) );

	register_sidebar( array(
		'name'          => __( 'Header Top Right Widget', 'skt-trust' ),
		'description'   => __( 'Appears on header top right area', 'skt-trust' ),
		'id'            => 'header-right-widget',
		'before_widget' => '<div class="header-top-right">',		
		'before_title'  => '<h3 style="display:none;">',
		'after_title'   => '</h3>',
		'after_widget'  => '</div>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar Main', 'skt-trust' ),
		'description'   => __( 'Appears on page sidebar', 'skt-trust' ),
		'id'            => 'sidebar-main',
		'before_widget' => '',		
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
	) );	

	register_sidebar( array(
		'name'          => __( 'Footer Widget 1', 'skt-trust' ),
		'description'   => __( 'Appears on footer', 'skt-trust' ),
		'id'            => 'footer-1',
		'before_widget' => '<div id="%1$s" class="widget-column-1">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Widget 2', 'skt-trust' ),
		'description'   => __( 'Appears on footer', 'skt-trust' ),
		'id'            => 'footer-2',
		'before_widget' => '<div id="%1$s" class="widget-column-2">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Footer Widget 3', 'skt-trust' ),
		'description'   => __( 'Appears on footer', 'skt-trust' ),
		'id'            => 'footer-3',
		'before_widget' => '<div id="%1$s" class="widget-column-3">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );
	
		register_sidebar( array(
		'name'          => __( 'Footer Widget 4', 'skt-trust' ),
		'description'   => __( 'Appears on footer', 'skt-trust' ),
		'id'            => 'footer-4',
		'before_widget' => '<div id="%1$s" class="widget-column-4">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar Contact Info', 'skt-trust' ),
		'description'   => __( 'Appears on contact page template', 'skt-trust' ),
		'id'            => 'sidebar-contact',
		'before_widget' => '<aside class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );		

}
add_action( 'widgets_init', 'skt_trust_widgets_init' );

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once get_template_directory() . '/inc/options-framework.php';

function skt_trust_scripts() {
	wp_enqueue_style( 'skt-trust-montserrat-gfonts', '//fonts.googleapis.com/css?family=Montserrat:400,700);' );
	wp_enqueue_style( 'skt-trust-robotocond-gfonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' );
	wp_enqueue_style( 'skt-trust-roboto-gfonts', '//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,700,500italic,700italic,900,900italic' );
	wp_enqueue_style( 'skt-trust-lato-gfonts', '//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic);)' );
	wp_enqueue_style( 'skt-trust-basic-style', get_stylesheet_uri() );
	wp_enqueue_style( 'skt-trust-editor-style', get_template_directory_uri().'/editor-style.css' );
	wp_enqueue_style( 'skt-trust-base-style', get_template_directory_uri().'/css/style_base.css' );	
	
	if ( is_home() || is_front_page() ) { 
	wp_enqueue_style( 'skt-trust-nivo-style', get_template_directory_uri().'/css/nivo-slider.css' );
	wp_enqueue_script( 'skt-trust-nivo-slider', get_template_directory_uri() . '/js/jquery.nivo.slider.js', array('jquery') );
	}	
	wp_enqueue_script( 'skt-trust-custom-scripts', get_template_directory_uri() . '/js/custom.js', array('jquery') );
	/**** Owl Testimonial Style/JS ***/
	wp_enqueue_script( 'skt-trust-testimonial-scripts', get_template_directory_uri().'/js/owl.carousel.js' );
	wp_enqueue_style( 'skt-trust-testimonial-style', get_template_directory_uri().'/css/owl.carousel.min.css' );		
	wp_enqueue_style( 'skt-trust-testimonial-default-style', get_template_directory_uri().'/css/owl.theme.default.min.css' );
	/**** Gallery Style/JS ***/
	wp_enqueue_style( 'skt-trust-prettyphoto-style', get_template_directory_uri().'/css/prettyPhoto.css' );
	wp_enqueue_script( 'skt-trust-prettyphoto-script', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery') );
	wp_enqueue_style( 'skt-trust-font-awesome-style', get_template_directory_uri().'/css/font-awesome.min.css' );	
	wp_enqueue_style( 'skt-trust-responsive-style', get_template_directory_uri().'/css/theme-responsive.css' );		
	//Mixit Up
	wp_enqueue_style( 'skt-trust-mixitup-style', get_template_directory_uri().'/mixitup/demo.css' );
	wp_enqueue_script( 'skt-trust-mixitup-script', get_template_directory_uri() . '/mixitup/jquery_mixitup.js', array('jquery') );
	
	//Accocdion Js
	wp_enqueue_script( 'skt-trust-jquery-ui-scripts', get_template_directory_uri().'/js/jquery-ui.js' );
	wp_enqueue_style( 'skt-trust-jquery-ui-style', get_template_directory_uri().'/css/jquery-ui.css' );

	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'skt_trust_scripts' );

function media_css_hook(){ ?>
<script>
jQuery.noConflict();
jQuery(window).load(function(){
  jQuery(".owl-carousel").owlCarousel({
    responsiveClass:true,
	items : 1,
	autoplaySpeed: <?php echo of_get_option('testirotatespeed',true); ?>,
	pause: <?php echo of_get_option('testimonialsautorotate',true); ?>,
	nav:false,
	smartSpeed:<?php echo of_get_option('testirotatespeed',true); ?>,
});
});
</script>

<?php if ( is_home() || is_front_page() ) { ?>        
<script>
		jQuery(window).load(function() {
        jQuery('#slider').nivoSlider({
        	effect:'<?php echo of_get_option('slideefect',true); ?>',
		  	animSpeed: <?php echo of_get_option('slideanim',true); ?>,
			pauseTime: <?php echo of_get_option('slidepause',true); ?>,
			directionNav: <?php echo of_get_option('slidenav',true); ?>,
			controlNav: <?php echo of_get_option('slidepage',true); ?>,
			pauseOnHover: <?php echo of_get_option('slidepausehover',true); ?>,
    });
});
</script>
<?php } ?>
<?php } 
add_action('wp_head','media_css_hook'); 


function skt_trust_custom_head_codes() {
	if ( (function_exists( 'of_get_option' )) && (of_get_option('style2', true) != 1) ) {
		echo "<style>". esc_html( of_get_option('style2', true) ) ."</style>";
	}
	if ( function_exists( 'of_get_option' )  )  {
			$typebody 		  		  = of_get_option('bodyfontface');
			$typelogo 		  		  = of_get_option('logotype');
			$typetag  		  		  = of_get_option('tagtype');
			$typenav  		  		  = of_get_option('navtype');
			$typeslidertitle		  = of_get_option('slidertitletype');
			$typesliderdesc	  		  = of_get_option('sliderdesctype');
			$typesectiontitle	  		  = of_get_option('sectiontitletype');
			$typefootertitle	  		  = of_get_option('footertitletype');
			$typeheadingh1	  		  = of_get_option('headingh1type');
			$typeheadingh2	  		  = of_get_option('headingh2type');
			$typeheadingh3	  		  = of_get_option('headingh3type');
			$typeheadingh4	  		  = of_get_option('headingh4type');
			$typeheadingh5	  		  = of_get_option('headingh5type');
			$typeheadingh6	  		  = of_get_option('headingh6type');
			
 		if( $typebody['face'] != '' && is_google_font( $typebody['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typebody['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typelogo['face'] != '' && is_google_font( $typelogo['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typelogo['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typetag['face'] != '' && is_google_font( $typetag['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typetag['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typenav['face'] != '' && is_google_font( $typenav['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typenav['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeslidertitle['face'] != '' && is_google_font( $typeslidertitle['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeslidertitle['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typesliderdesc['face'] != '' && is_google_font( $typesliderdesc['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typesliderdesc['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typesectiontitle['face'] != '' && is_google_font( $typesectiontitle['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typesectiontitle['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typefootertitle['face'] != '' && is_google_font( $typefootertitle['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typefootertitle['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh1['face'] != '' && is_google_font( $typeheadingh1['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh1['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh2['face'] != '' && is_google_font( $typeheadingh2['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh2['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh3['face'] != '' && is_google_font( $typeheadingh3['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh3['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh4['face'] != '' && is_google_font( $typeheadingh4['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh4['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh5['face'] != '' && is_google_font( $typeheadingh5['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh5['face'])."' rel='stylesheet' type='text/css' />";
		}
		if( $typeheadingh6['face'] != '' && is_google_font( $typeheadingh6['face'] ) ){
			echo "<link href='http://fonts.googleapis.com/css?family=".str_replace(' ','+',$typeheadingh6['face'])."' rel='stylesheet' type='text/css' />";
		}
		echo "<style>";
			
// Header Top 

	if( of_get_option('headertopbgcolor',true) != ''){
		echo ".header-top {background:".of_get_option('headertopbgcolor').";}";
	}
	if( of_get_option('headertoptextcolor',true) != ''){
		echo ".header-top, .header-top a {color:".of_get_option('headertoptextcolor').";}";
	}
	if( of_get_option('headertoptextcolor',true) != ''){
		echo ".header-top .social-icons a, .header-top .social-icons {color:".of_get_option('headertoptextcolor')."; border-color:".of_get_option('headertoptextcolor').";}";
	}

	if( of_get_option('headertoptextcolor',true) != ''){
		echo ".header-top .social-icons a:hover {background:".of_get_option('headertoptextcolor').";}";
	}

// Header 
	if( of_get_option('headerbgcolor',true) != ''){
		echo ".header .header-inner{background:".of_get_option('headerbgcolor').";}";
	}

	if( of_get_option('bodyfontface',true) != '' || of_get_option('bodybgcolor' , true)){
		echo "body{font-family:".$typebody['face']."; font-size:".$typebody['size']."; color:".$typebody['color']."; font-weight:".$typebody['weight']."; text-transform:".$typebody['transform']."; font-style:".$typebody['style']."; background:".of_get_option('bodybgcolor', true).";}";
	}

	if( of_get_option('headertopbgcolor',true) != ''){
		echo ".toggle a{background-color:".of_get_option('headertopbgcolor').";}";
	}

	if( of_get_option('bodyfontface',true) != ''){
		echo ".content-area .phone-no a, .admin-post a, .our-testimonials .testimonial-content{color:".$typebody['color'].";}";
	}
	
	if( of_get_option('logotype',true) != ''){
		echo ".header .header-inner .logo h2{font-family:".$typelogo['face']."; font-size:".$typelogo['size']."; color:".$typelogo['color']."; font-weight:".$typelogo['weight']."; text-transform:".$typelogo['transform']."; font-style:".$typelogo['style'].";}";
	}
	if( of_get_option('tagtype',true) != ''){
		echo ".header .header-inner .logo a span.tagline{font-family:".$typetag['face']."; font-size:".$typetag['size']."; color:".$typetag['color']."; font-weight:".$typetag['weight']."; text-transform:".$typetag['transform']."; font-style:".$typetag['style'].";}";
	}
	if( of_get_option('logoheight',true) != '' ){
			echo ".header .header-inner .logo img{height:".of_get_option('logoheight',true)."px;}";
	}

// Header Navigation
	if( of_get_option('navtype',true) != ''){
		echo ".header .header-inner .nav ul li a {font-family:".$typenav['face']."; font-size:".$typenav['size']."; color:".$typenav['color']."; font-weight:".$typenav['weight']."; text-transform:".$typenav['transform']."; font-style:".$typenav['style'].";}";
	}
	if( of_get_option('navtype',true) != '' || of_get_option('navigationdrbtbrcolor',true) != ''){
		echo ".header .header-inner .nav ul li ul li a, .header .header-inner .nav ul li.current_page_item ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a {color:".$typenav['color']."; border-color:".of_get_option('navigationdrbtbrcolor', true).";}";
	}

	if ( of_get_option('navigationhovercolor', true) != '' || of_get_option('navigationhoverbgcolor', true) != '' ) {
		echo ".header .header-inner .nav ul li a:hover, .header .header-inner .nav ul li.current_page_item a, .header .header-inner .nav ul li.current_page_item ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.current_page_item a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li.current_page_item a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children.current_page_ancestor a, .header .header-inner .nav ul li.current_page_ancestor a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_item a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li.current_page_item a {background-color: ".of_get_option('navigationhoverbgcolor', true)."; color:".of_get_option('navigationhovercolor', true).";}";
		}
	if ( of_get_option('navigationdrbgcolor', true) != '') {
		echo ".header .header-inner .nav ul li ul li a, .header .header-inner .nav ul li.current_page_item ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a {background-color: ".of_get_option('navigationdrbgcolor', true).";}";
		}

	if ( of_get_option('navigationhovercolor', true) != '' || of_get_option('navigationhoverbgcolor', true) != '' ) {
		echo "@media screen and (max-width:1023px){.header .header-inner .nav ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor a:hover {background-color: ".of_get_option('navigationhoverbgcolor', true)." !important; color:".of_get_option('navigationhovercolor', true)." !important;}}";
		}
		
	if( of_get_option('navtype',true) != ''){
		echo "@media screen and (max-width:1023px){.header .header-inner .nav ul li a {color:".$typenav['color']." !important;}}";
	}

// Slider Setting

	if( of_get_option('slidertitletype',true) != ''){
		echo ".slide_info h2{font-family:".$typeslidertitle['face'].", cursive; font-size:".$typeslidertitle['size']."; color:".$typeslidertitle['color']."; font-weight:".$typeslidertitle['weight']."; text-transform:".$typeslidertitle['transform']."; font-style:".$typeslidertitle['style'].";}";
	}

	if( of_get_option('sliderdesctype',true) != ''){
		echo ".slide_info {font-family:".$typesliderdesc['face']."; font-size:".$typesliderdesc['size']."; color:".$typesliderdesc['color']."; font-weight:".$typesliderdesc['weight']."; text-transform:".$typesliderdesc['transform']."; font-style:".$typesliderdesc['style'].";}";
	}	

	if( of_get_option('sliderdesctype',true) != ''){
		echo ".slide_info h3 {color:".$typesliderdesc['color']." !important;}";
	}	

	if ( of_get_option('sliderbutbrcolor', true) != '' ||  of_get_option('sliderbutcolor', true) != '') {
			echo 'a.sldbutton{ border-color:'. esc_html( of_get_option('sliderbutbrcolor', true) ) .';  color:'. esc_html( of_get_option('sliderbutcolor', true) ) .' !important; }';
	}

	if ( of_get_option('sliderbutbghvcolor', true) != '' || of_get_option('sliderbuttexthvcolor')) {
			echo 'a.sldbutton:hover{ background-color:'. esc_html( of_get_option('sliderbutbghvcolor', true) ) .'; color:'. esc_html( of_get_option('sliderbuttexthvcolor', true) ) .' !important; border-color:'. esc_html( of_get_option('sliderbutbghvcolor', true) ) .';}';
	}	

// Slider controls colors		
	if ( of_get_option('sldnavbg', true) != '' ) {
			echo ".nivo-directionNav a{background-color:".of_get_option('sldnavbg', true).";}";
		}
			
	if( of_get_option('sldnavhvbg',true) != ''){
		echo '.nivo-directionNav a:hover{background-color:'. esc_html(of_get_option('sldnavhvbg',true) ).';}';
	}
	if( of_get_option('sldpagebg',true) != ''){
		echo ".nivo-controlNav a, ol.nav-numbers li a, .owl-theme .owl-dots .owl-dot span{background-color:".of_get_option('sldpagebg',true).";}";
	}
	if( of_get_option('sldpagehvbg',true) != ''){
		echo ".nivo-controlNav a.active, ol.nav-numbers li.active a, .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span{background-color:".of_get_option('sldpagehvbg',true).";}";
	}	


// Why Choose Us
	if( of_get_option('whychoosetextcolor',true) != ''){
		echo ".choose-thumb {border-color:".of_get_option('whychoosetextcolor',true).";}";
	}	

	if( of_get_option('whychooseiconcolor',true) != ''){
		echo ".choose-col i, .choose-col {color:".of_get_option('whychooseiconcolor',true).";}";
	}	

	if( of_get_option('whychoosetextcolor',true) != ''){
		echo ".choose-col:hover i{color:".of_get_option('whychoosetextcolor',true).";}";
	}	

// Events
	if( of_get_option('eventtextcolor',true) != ''){
		echo ".event-content {color:".of_get_option('eventtextcolor',true).";}";
	}	

	if( of_get_option('eventdttextcolor',true) != '' || of_get_option('eventdtbgcolor',true)){
		echo ".event-thumb h3{color:".of_get_option('eventdttextcolor',true)."; background-color:".of_get_option('eventdtbgcolor',true).";}";
	}	

	if( of_get_option('causestextcolor',true) != ''){
		echo ".causes-content, .cuase-raised span, .cuase-goal span  {color:".of_get_option('causestextcolor',true).";}";
	}	

	if( of_get_option('causespercentarrow',true) != ''){
		echo ".causes-col .skill-bar-percent::after {border-top-color:".of_get_option('causespercentarrow',true).";}";
	}	

	if( of_get_option('sldpagehvbg',true) != ''){
		echo ".our-testimonials i{color:".of_get_option('sldpagehvbg',true).";}";
	}	

// Client Logoclientlogobrcolor
	if( of_get_option('clientlogobgcolor',true) != '' ){
		echo ".client-logo {background:".of_get_option('clientlogobgcolor',true)."; border-color:".of_get_option('clientlogobgcolor',true).";}";
	}	
	if(of_get_option('clientlogobrcolor',true) != '' ){
		echo ".client-logo:hover {border-color:".of_get_option('clientlogobrcolor',true).";}";
	}	

// Section title color	
	if( of_get_option('sectiontitletype',true) != ''){
		echo "h2.section_title, .professional-content h2 {font-family:".$typesectiontitle['face']."; font-size:".$typesectiontitle['size']."; color:".$typesectiontitle['color']."; font-weight:".$typesectiontitle['weight']."; text-transform:".$typesectiontitle['transform']."; font-style:".$typesectiontitle['style'].";}";
	}	

//testimonial color
	if( of_get_option('testimonialbgcolor',true) != ''){
		echo "#testimonial-section .testimonial-content, #testimonial-section .testimonial-content h5, section .testimonial-content, section .testimonial-content h5 {color:".of_get_option('testimonialbgcolor',true).";}";
	}	

//Gallery
	if( of_get_option('gallerynavigationcolor',true) != '' || of_get_option('gallerynavbgcolor',true) != ''){
		echo ".controls li {color:".of_get_option('gallerynavigationcolor',true)."; background:".of_get_option('gallerynavbgcolor',true).";}";
	}	
	if( of_get_option('gallerynavtxtcolor',true) != '' ||  of_get_option('gallerynavbgcolor',true) != ''){
		echo ".controls li.active, .controls li:hover {background:".of_get_option('gallerynavbgcolor',true)."; color:".of_get_option('gallerynavtxtcolor',true).";}";
	}	
	if( of_get_option('gallerybgcolor',true) != '' ){
		echo "#Grid .mix {background-color:".of_get_option('gallerybgcolor',true).";}";
	}	

// All site anchor tag
	if( of_get_option('anchortagcolor',true) != '' ){
		echo "a, .footer a:hover, .recent-post:hover h5 , .copyright a:hover, .footer .cols-1 .widget-column-1 ul li a:hover, .cols-1 .widget-column-1 ul li.current_page_item a, .footer ul li.current_page_item, .footer ul li:hover a, .footer ul li:hover:before, .footer .phone-no a:hover, .woocommerce table.shop_table th, .woocommerce-page table.shop_table th, .post-cate, .news-box h2:hover, .news-posts-share:hover, .servies-col i, .professional-content li a:hover, .one_four_page:hover h4, .admin-post a:hover, .choose-col:hover i  {color:".of_get_option('anchortagcolor',true).";}";
	}
	
		if( of_get_option('anchortagcolorhover',true) != '' ){
		echo "a:hover, .professional-content li a{color:".of_get_option('anchortagcolorhover',true).";}";
	}
// Default Social color/background
	if( of_get_option('socialfontcolor',true) != '' || of_get_option('socialbgcolor',true) != '' ){
		echo ".social-icons a{color:".of_get_option('socialfontcolor',true)."; background:".of_get_option('socialbgcolor',true).";}";
	}
	
	if( of_get_option('socialbghvcolor',true) != '' || of_get_option('socialfonthvcolor',true) != ''){
		echo ".social-icons a:hover {background:".of_get_option('socialbghvcolor',true)."; color:".of_get_option('socialfonthvcolor',true).";}";
	}

// Default Button contact/blog/single comment
	if( of_get_option('btntextcolor',true) != '' || of_get_option('btnbgcolor', true) != '' || of_get_option('btnbordercolor', true) != ''){
		echo ".view-all-btn a, #contactform_main input[type='submit']:hover, .wpcf7 form input[type='submit'], input[type='submit'], .button:hover, #commentform input#submit, input.search-submit, .post-password-form input[type=submit], .read-more, .pagination .current, .pagination a:hover, .compatible .read-more, .accordion-section .ui-accordion .ui-accordion-header span.fa, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, a.added_to_cart:hover, .causes-col .causes-thumb a.read-more:hover {background-color:".of_get_option('btnbgcolor',true)."; color:". of_get_option('btntextcolor', true) .";}";
	}
	if( of_get_option('btntxthovercolor',true) != '' || of_get_option('btnhoverbgcolor', true) != ''){
		echo ".view-all-btn a:hover, #contactform_main input[type='submit']:hover, .wpcf7 form input[type='submit']:hover, input[type='submit']:hover, .button:hover, #commentform input#submit:hover, input.search-submit:hover, .post-password-form input[type=submit]:hover, .read-more:hover , .accordion-box h2:before, .pagination span, .pagination a, .luxury-experience a:hover, a.added_to_cart, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .slider-form  h3:after, .compatible .read-more:hover, .one_four_page:hover .read-more, .accordion-section .ui-accordion .ui-accordion-header.ui-accordion-header-active span.fa, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .causes-col .causes-thumb a.read-more {background-color:".of_get_option('btnhoverbgcolor',true)."; color:". esc_html( of_get_option('btntxthovercolor', true) ) .";}";}


// Footer Color Setting 
	if( of_get_option('footerwpbgcolor',true) != '' || of_get_option('footertopbrcolor',true) != ''){
		echo "#footer-wrapper{background-color:".of_get_option('footerwpbgcolor',true)."; border-color:".of_get_option('footertopbrcolor',true).";}";
	}

	if( of_get_option('footerwpcolor',true) != '' ){
		echo "#footer-wrapper, .footer .recent-post h6 {color:".of_get_option('footerwpcolor',true).";}";
	}

	if( of_get_option('copyrightbgcolor',true) != '' || of_get_option('copyrighttxcolor') != ''){
		echo ".copyright{background-color:".of_get_option('copyrightbgcolor',true)."; color:".of_get_option('copyrighttxcolor').";}";
	}
	if( of_get_option('fttitletype',true) != ''){
		echo ".footer h5{font-family:".$typefootertitle['face']."; font-size:".$typefootertitle['size']."; color:".$typefootertitle['color']."; font-weight:".$typefootertitle['weight']."; text-transform:".$typefootertitle['transform']."; font-style:".$typefootertitle['style'].";}";
	}

// Sidebar option
	if( of_get_option('widgettitlebgcolor',true) != '' || of_get_option('widgettitlecolor',true) != '' ){
		echo "h3.widget-title, .searchbox-icon, #sidebar .woocommerce-product-search input[type='submit']{background-color:".of_get_option('widgettitlebgcolor',true)."; color:".of_get_option('widgettitlecolor',true).";}";
	}

// Sidebar option
	if( of_get_option('widgettitlecolor',true) != '' ){
		echo "h3.widget-title, .contact-page .site-main h3{ color:".of_get_option('widgettitlecolor',true).";}";
	}

	if( of_get_option('sidebarbgcolor',true) != '' ){
		echo "aside.widget, .comment-form-comment textarea, .our-testimonials, .tm_description {background-color:".of_get_option('sidebarbgcolor',true)." !important;}";
	}
	if( of_get_option('sidebarbgcolor',true) != '' ){
		echo ".tm_description::after {border-top-color:".of_get_option('sidebarbgcolor',true)." !important;}";
	}
	if( of_get_option('sidebarliaborder',true) != '' ){
		echo "#sidebar ul li, #sidebar .home-sidebar ul li ul li{border-color:".of_get_option('sidebarliaborder',true).";}";
	}
	if( of_get_option('sidebarlnkcolor',true) != '' ){
		echo "#sidebar ul li a, .category-post li a, .searchbox-input, .searchbox-input::-moz-placeholder {color:".of_get_option('sidebarlnkcolor',true).";}";
	}
	if( of_get_option('searchbgcolor',true) != ''){
		echo ".searchbox-input{background-color:".of_get_option('searchbgcolor',true).";}";
	}

//input field
	if( of_get_option('inputbgcolor',true) != '' ||  of_get_option('inputtextbgcolor',true) != '' || of_get_option('inputtextborcolor',true) != ''){
		echo "#contactform_main input[type='text'], #contactform_main input[type='email'], #contactform_main input[type='tel'], #contactform_main input[type='url'], #contactform_main textarea, .wpcf7 form input[type='text'], .wpcf7 form input[type='email'], .wpcf7 form input[type='tel'], .wpcf7 form textarea, select, input.appointfield[type='text'], #sidebar select, input[type='text'], input[type='email'], input[type='tel'], textarea {background-color:".of_get_option('inputbgcolor',true)."; color:".of_get_option('inputtextbgcolor',true)."; border-color:".of_get_option('inputtextborcolor',true).";}";
		}

//input field
	if(of_get_option('inputtextborcolor',true) != ''){
		echo ".accordion-section .ui-accordion .ui-accordion-header, .accordion-section .ui-accordion .ui-accordion-content {border-color:".of_get_option('inputtextborcolor',true).";}";
		}

// Heading Tag
	if( of_get_option('headingh1type',true) != ''){
		echo "h1{font-family:".$typeheadingh1['face']."; font-size:".$typeheadingh1['size']."; color:".$typeheadingh1['color']."; font-weight:".$typeheadingh1['weight']."; text-transform:".$typeheadingh1['transform']."; font-style:".$typeheadingh1['style'].";}";
	}
	if( of_get_option('headingh2type',true) != ''){
		echo "h2{font-family:".$typeheadingh2['face']."; font-size:".$typeheadingh2['size']."; color:".$typeheadingh2['color']."; font-weight:".$typeheadingh2['weight']."; text-transform:".$typeheadingh2['transform']."; font-style:".$typeheadingh2['style'].";}";
	}
	if( of_get_option('headingh3type',true) != ''){
		echo "h3{font-family:".$typeheadingh3['face']."; font-size:".$typeheadingh3['size']."; color:".$typeheadingh3['color']."; font-weight:".$typeheadingh3['weight']."; text-transform:".$typeheadingh3['transform']."; font-style:".$typeheadingh3['style'].";}";
	}
	if( of_get_option('headingh4type',true) != ''){
		echo "h4{font-family:".$typeheadingh4['face']."; font-size:".$typeheadingh4['size']."; color:".$typeheadingh4['color']."; font-weight:".$typeheadingh4['weight']."; text-transform:".$typeheadingh4['transform']."; font-style:".$typeheadingh4['style'].";}";
	}
	if( of_get_option('headingh5type',true) != ''){
		echo "h5{font-family:".$typeheadingh5['face']."; font-size:".$typeheadingh5['size']."; color:".$typeheadingh5['color']."; font-weight:".$typeheadingh5['weight']."; text-transform:".$typeheadingh5['transform']."; font-style:".$typeheadingh5['style'].";}";
	}
	if( of_get_option('headingh6type',true) != ''){
		echo "h6{font-family:".$typeheadingh6['face']."; font-size:".$typeheadingh6['size']."; color:".$typeheadingh6['color']."; font-weight:".$typeheadingh6['weight']."; text-transform:".$typeheadingh6['transform']."; font-style:".$typeheadingh6['style'].";}";
	}
	
// Woocommerce price Filter
	if ( of_get_option('pricesliderbg', true) != '' ) {
			echo '#sidebar .price_slider_wrapper .ui-widget-content{background-color:'. esc_html( of_get_option('pricesliderbg', true) ) .';}';
	}
	if ( of_get_option('pricehandlebg', true) != '' ) {
			echo '#sidebar .ui-slider .ui-slider-handle{background-color:'. esc_html( of_get_option('pricehandlebg', true) ) .';}';
	}
	if ( of_get_option('pricerangebg', true) != '' ) {
			echo '#sidebar .ui-slider .ui-slider-range{background-color:'. esc_html( of_get_option('pricerangebg', true) ) .';}';
	}
		
		echo "</style>";
	} 

}	
add_action('wp_head', 'skt_trust_custom_head_codes');


function skt_trust_pagination() {
	global $wp_query;
	$big = 12345678;
	$page_format = paginate_links( array(
	    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	    'format' => '?paged=%#%',
	    'current' => max( 1, get_query_var('paged') ),
	    'total' => $wp_query->max_num_pages,
	    'type'  => 'array'
	) );
	if( is_array($page_format) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
		echo '<div class="pagination"><div><ul>';
		echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
		foreach ( $page_format as $page ) {
			echo "<li>$page</li>";
		}
		echo '</ul></div></div>';
	}
}
/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/**
 * Load custom functions file.
 */
require get_template_directory() . '/inc/custom-functions.php';


function skt_trust_blogpost_pagination( $wp_query ){
	$big = 999999999; // need an unlikely integer
	if ( get_query_var('paged') ) { $pageVar = 'paged'; }
	elseif ( get_query_var('page') ) { $pageVar = 'page'; }
	else { $pageVar = 'paged'; }
	$pagin = paginate_links( array(
		'base' 			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' 		=> '?'.$pageVar.'=%#%',
		'current' 		=> max( 1, get_query_var($pageVar) ),
		'total' 		=> $wp_query->max_num_pages,
		'prev_text'		=> '&laquo; Prev',
		'next_text' 	=> 'Next &raquo;',
		'type'  => 'array'
	) ); 
	if( is_array($pagin) ) {
		$paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
		echo '<div class="pagination"><div><ul>';
		echo '<li><span>'. $paged . ' of ' . $wp_query->max_num_pages .'</span></li>';
		foreach ( $pagin as $page ) {
			echo "<li>$page</li>";
		}
		echo '</ul></div></div>';
	} 
}

function skt_trust_get_slug_by_id($id) {
	$post_data = get_post($id, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug; 
}

/* css boo*/
