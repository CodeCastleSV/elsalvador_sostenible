<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package SKT Trust
 */

$footerlayout = of_get_option('footerlayout');
 
?>

    
    <div id="footer-wrapper">
          <div class="container">
            <div class="footer">
              <?php if($footerlayout=='onecolumn'){ ?>
              <div class="cols-1">
                <?php if(!dynamic_sidebar('footer-1')) : ?>
                <div class="widget-column-1">
                  <?php wp_nav_menu( array( 'theme_location' => 'footer') ); ?>
                  <div class="clear"></div>
                </div>
                <?php  endif; ?>
                <div class="clear"></div>
              </div>
              <?php 
        }
         elseif ($footerlayout=='twocolumn'){ ?>
              <div class="cols-2">
                <?php if(!dynamic_sidebar('footer-1')) : ?>
                <div class="widget-column-1">
                  <h5><?php if( of_get_option('footerabouttexttitle') != '') { echo of_get_option('footerabouttexttitle'); } ; ?></h5>
                  <?php echo do_shortcode(of_get_option('footeraboutcontent', true)) ; ?>
                  <div class="clear"></div>
                </div>
                <?php  endif; ?>
                <?php if(!dynamic_sidebar('footer-2')) : ?>
                <div class="widget-column-2">
                  <h5><?php if( of_get_option('footerrcposttitle') != '') { echo of_get_option('footerrcposttitle'); } ; ?></h5>
                  <?php echo do_shortcode('[footer-posts show="3"] ');?>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
              </div>
              <!--end .cols-2-->
              <?php 
        }
        elseif($footerlayout=='threecolumn'){ ?>
              <div class="cols-3">
                <?php if(!dynamic_sidebar('footer-1')) : ?>
                <div class="widget-column-1">
                  <h5><?php if( of_get_option('footerabouttexttitle') != '') { echo of_get_option('footerabouttexttitle'); } ; ?></h5>
                  <?php echo do_shortcode(of_get_option('footeraboutcontent', true)) ; ?>
                  <div class="clear"></div>
                </div>
                <?php  endif; ?>
                <?php if(!dynamic_sidebar('footer-2')) : ?>
                <div class="widget-column-2">
                  <h5><?php if( of_get_option('footerrcposttitle') != '') { echo of_get_option('footerrcposttitle'); } ; ?></h5>
                  <?php echo do_shortcode('[footer-posts show="3"] ');?>
                </div>
                <?php endif; ?>
                <?php if(!dynamic_sidebar('footer-3')) : ?>
                <div class="widget-column-3"><h5><?php if( of_get_option('contacttitle') != ''){ echo of_get_option('contacttitle');}; ?></h5>
                  <p class="parastyle">
                    <?php if( of_get_option('address',true) != '') { echo of_get_option('address',true) ; } ; ?>
                    <?php if( of_get_option('address2',true) != '') { echo '<br />'; echo of_get_option('address2',true) ; } ; ?>
                  </p>
                  <div class="phone-no">
                    <p><?php if( of_get_option('phone',true) != ''){ ?><?php echo of_get_option('phone'); ?><?php } ?>
                    <span class="phno2"><?php if( of_get_option('phone2',true) != ''){ ?><?php echo of_get_option('phone2'); ?><?php } ?></span></p>
                    <p><?php if( of_get_option('email',true) != '' ) { ?><?php echo of_get_option('email',true) ; ?><?php } ?></p>
                    <?php if( of_get_option('weblink',true) != '' ) { ?><p><?php echo of_get_option('weblink',true) ; ?></p><?php } ?>
                  </div>
                  <div class="clear"></div>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
              </div>
              <!--end .cols-3-->
              <?php
        }
        elseif($footerlayout=='fourcolumn'){ ?>
              <div class="cols-4">
                <?php if(!dynamic_sidebar('footer-1')) : ?>
                <div class="widget-column-1">
                  <h5><?php if( of_get_option('footerabouttexttitle') != '') { echo of_get_option('footerabouttexttitle'); } ; ?></h5>
                  	 <?php echo do_shortcode(of_get_option('footeraboutcontent', true)) ; ?>
                </div>
                <?php  endif; ?>
                
                <?php if(!dynamic_sidebar('footer-2')) : ?>
                <div class="widget-column-2">
                  <h5><?php if( of_get_option('footerrcposttitle') != '') { echo of_get_option('footerrcposttitle'); } ; ?></h5>
                  <?php echo do_shortcode('[footer-posts show="3"] ');?>
                </div>
                <?php endif; ?>
                <?php if(!dynamic_sidebar('footer-3')) : ?>
				<div class="widget-column-3">
                  <h5><?php if( of_get_option('footertwitttitle') != '') { echo of_get_option('footertwitttitle'); } ; ?></h5>
                  <?php echo do_shortcode(of_get_option('footertwittercode')); ?>
                 </div>                
                <?php endif; ?>
                <?php if(!dynamic_sidebar('footer-4')) : ?>
                <div class="widget-column-4">
                  <h5>
                    <?php if( of_get_option('contacttitle') != ''){ echo of_get_option('contacttitle');}; ?>
                  </h5>
                  <p class="parastyle">
                    <?php if( of_get_option('address',true) != '') { echo of_get_option('address',true) ; } ; ?>
                    <?php if( of_get_option('address2',true) != '') { echo '<br />'; echo of_get_option('address2',true) ; } ; ?>
                  </p>
                  <div class="phone-no">
                    <p><span class="phno"><?php if( of_get_option('phone',true) != ''){ ?><?php echo of_get_option('phone'); ?><?php } ?></span>
                    <span class="phno2"><?php if( of_get_option('phone2',true) != ''){ ?><?php echo of_get_option('phone2'); ?><?php } ?></span></p>
                    <p><?php if( of_get_option('email',true) != '' ) { ?><?php echo of_get_option('email',true) ; ?><?php } ?></p>
                    <?php if( of_get_option('weblink',true) != '' ) { ?><p><?php echo of_get_option('weblink',true) ; ?></p><?php } ?>
                  </div>
                  <?php echo do_shortcode(of_get_option('footersocialmedia', true)) ; ?>
                  <div class="clear"></div>
                </div>
                <?php endif; ?>
                <div class="clear"></div>
              </div>
              <!--end .cols-4-->
              <?php } ?>
            </div>
            <!--end .footer--> 
      
      
      </div><!--end .container-->
      
      <div class="copyright">
      	<div class="container">
          <div class="copyright-txt">
            <?php if( of_get_option('copytext',true) != ''){ echo of_get_option('copytext',true); }; ?>
          </div>
          <div class="design-by">
            <?php if( of_get_option('ftlink', true) != ''){echo of_get_option('ftlink',true);}; ?>
          </div>
        <div class="clear"></div>
      </div><!-- copyright -->
      </div>
      
    </div><!-- footer-wrapper -->
<?php wp_footer(); ?>
<div class="clear"></div>
</div>
<!-- share this -->
<script type="text/javascript">stLight.options({publisher: "74377c55-a770-4d9e-9dc9-60d99575c1f8", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</body>
</html>