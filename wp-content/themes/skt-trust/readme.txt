/*-----------------------------------------------------------------------------------*/
/* SKT non profit WordPress Theme */
/*-----------------------------------------------------------------------------------*/

Theme Name      :   SKT Trust
Theme URI       :   https://www.sktthemes.net/shop/non-profit-wordpress-theme
Version         :   pro1.0
Tested up to    :   WP 4.5.3
Author          :   SKT Themes
Author URI      :   https://www.sktthemes.net/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*-----------------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*-----------------------------------------------------------------------------------*/

email       :   support@sktthemes.com

/*-----------------------------------------------------------------------------------*/
/* Theme Resources */
/*-----------------------------------------------------------------------------------*/

Theme is Built using the following resource bundles.

1 - All js that have been used are within folder /js of theme.
- jquery.easing.min.js is licensed under BSD.
- jquery.prettyPhoto.js is licensed under GPLv2.
- html5.js is dual licensed under MIT and GPL2.

2 - 	Fonts used in this theme are defined below

	Lato : https://www.google.com/fonts/specimen/Lato
	License: Distributed under the terms of the Apache License, version 2.0 		

http://www.apache.org/licenses/LICENSE-2.0.html

3 - 	Images used from Pixabay.
	Pixabay provides images under CC0 license 		(https://creativecommons.org/about/cc0)

	
	Slides:
		http://pixabay.com/en/child-boy-baby-poor-person-indio-207573/
		https://pixabay.com/en/school-india-children-ahmedabad-330580/
		https://pixabay.com/en/children-infant-girl-school-306607/

	Other Images:
	http://pixabay.com/en/children-afghanistan-afghani-girl-63175/
        http://pixabay.com/en/girl-dog-poor-dirty-children-62328/
        http://pixabay.com/en/haiti-children-boys-girls-reaching-79641/
        http://pixabay.com/en/market-korea-seoul-vendors-344598/
        http://pixabay.com/en/tanga-tanzania-boy-child-close-up-102577/
        http://pixabay.com/en/help-support-poverty-street-473986/
        http://pixabay.com/en/burkina-faso-africa-kids-children-254158/
        http://pixabay.com/en/school-india-children-ahmedabad-330580/
        http://pixabay.com/en/kids-india-volunteers-674513/
        http://pixabay.com/en/man-smiling-shirt-young-sitting-438081/
        http://pixabay.com/en/man-outside-smiling-outdoors-happy-438083/
        http://pixabay.com/en/girl-people-landscape-sun-657753/
        http://pixabay.com/en/young-man-person-guy-handsome-507297/
		
For any help you can mail us at support[at]sktthemes.com