
<?php
/**
 * @package SKT Trust
 */
?>
<div class="blog-post-repeat">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <header class="entry-header">
            <h3 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
            <?php if ( 'post' == get_post_type() ) : ?>
                <div class="postmeta">
                    <div class="admin-post"><i class="fa fa-user"></i> <?php _e('By ','skt-trust'); the_author_posts_link(); ?></div>
                    <span class="post-date"><i class="fa fa-calendar"></i><?php _e('on ','skt-trust'); the_time('d F Y') ?></span>
                    <div class="clear"></div>
                </div><!-- postmeta -->
            <?php endif; ?>
	        <?php if ( is_search() || !is_single() ) : // Only display Excerpts for Search ?>
	            <div class="post-thumb archive-thumb"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('medium', array('class' => 'alignleft') ); ?></a></div>
	        <?php else : ?>
	            <div class="post-thumb"><?php the_post_thumbnail(); ?> </div>
	        <?php endif; ?>
        </header><!-- .entry-header -->
    
        <?php if ( is_search() || !is_single() ) : // Only display Excerpts for Search ?>
            <div class="entry-summary">
                <?php // the_excerpt(); ?>
                <?php the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>" class="read-more"><?php echo of_get_option('readmoretext'); ?></a>
            </div><!-- .entry-summary -->
        <?php else : ?>
            <div class="entry-content">
                <?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'skt-trust' ) ); ?>
                <?php
                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . __( 'Pages:', 'skt-trust' ),
                        'after'  => '</div>',
                    ) );
                ?>
            </div><!-- .entry-content -->
        <?php endif; ?>        
    </article><!-- #post-## -->
    <div class="spacer20"></div>
</div><!-- blog-post-repeat -->