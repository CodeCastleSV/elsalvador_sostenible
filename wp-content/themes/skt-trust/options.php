<?php 

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */ 

function optionsframework_option_name() {
	// Change this to use your theme slug
	$themename = wp_get_theme();
	$themename = preg_replace("/\W/", "_", strtolower($themename) );
	return $themename; 
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'skt-trust' 
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
*/

function optionsframework_options() { 

	// Pull all the pages into an array
	 $options_pages = array();
	 $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	 $options_pages[''] = 'Select a page:';
	 foreach ($options_pages_obj as $page) {
	  $options_pages[$page->ID] = $page->post_title;
	 }

	// array of section content.
	$section_text = array(
		1 => array(
			'section_title'		=> '',
			'menutitle'			=> '',
			'bgcolor' 			=> '#f7f7f7',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[column_row][column_content type="one_half" class="" background=""]
<h2>How Can Help You?</h2>[accordion_section][accordion title="Media" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam."][/accordion][accordion title="Becone Volunteer" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam."][/accordion][accordion title="Send Donation" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam."][/accordion][/accordion_section][/column_content][column_content type="one_half_last" class="" background=""]<h2>Donation Count</h2>[skillbar_section][skillbar skillbar_bgcolor="#1e1e1e" title="Children to get Shelter" title_color="#000000" content="$78,354.00/ $1,26,500.00" content_color="#383939" percent="61.94%" percent_color="#ffffff" percent_bgcolor="#ffd800"][skillbar skillbar_bgcolor="#1e1e1e" title="Girls to get Education" title_color="#000000" content="$78,354.00/ $1,26,500.00" content_color="#383939" percent="73.64%" percent_color="#ffffff" percent_bgcolor="#ffd800"][skillbar skillbar_bgcolor="#1e1e1e" title="Send Vegetables to Africa" title_color="#000000" content="$78,354.00/ $1,26,500.00" content_color="#383939" percent="86.58%" percent_color="#ffffff" percent_bgcolor="#ffd800"][/skillbar_section][/column_content][/column_row]',),

		2 => array(
			'section_title'		=> 'Our Causes',
			'menutitle'			=> '',
			'bgcolor' 			=> '',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[causes_main][our_causes image="'.get_template_directory_uri().'/images/causes-thumb1.jpg" title="charity activity of the year" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod." skillbar_bgcolor="#eeeeee" percent="12%" percent_color="#000000" percent_bgcolor="#ffd800" raised_title="Raised" raised_price="$12" goal_title="Goal" goal_price="$50.00" donate_button="Donate Now" link="#"][our_causes image="'.get_template_directory_uri().'/images/causes-thumb2.jpg" title="food and shelter for orphans" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod." skillbar_bgcolor="#eeeeee" percent="90%" percent_color="#000000" percent_bgcolor="#ffd800" raised_title="Raised" raised_price="$40.00" goal_title="Goal" goal_price="$50.00" donate_button="Donate Now" link="#"][our_causes image="'.get_template_directory_uri().'/images/causes-thumb3.jpg" title="education in africa" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod." skillbar_bgcolor="#eeeeee" percent="50%" percent_color="#000000" percent_bgcolor="#ffd800" raised_title="Raised" raised_price="$25.00" goal_title="Goal" goal_price="$50.00" donate_button="Donate Now" link="#" class="last"][/causes_main]'
		),

		3 => array(
			'section_title'		=> 'Why Choose Us',
			'menutitle'			=> '',
			'bgcolor' 			=> '#f7f7f7',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[choose_us_main][Choose_us icon="paper-plane" image="" link="" title="Guareanteed Results" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][Choose_us icon="user" image="" link="" title="Your Charitable Life" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][Choose_us icon="tachometer" image="" link="" title="No Goal Reuirements" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][Choose_us icon="heart" image="" link="" title="Most Trusted" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][Choose_us icon="credit-card-alt" image="" link="" title="Maximize Tax Advantages" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][Choose_us icon="star" image="" link="" title="Our Experience" content="Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem."][/choose_us_main]'
		),

		4 => array(
			'section_title'		=> 'Upcoming Events',
			'menutitle'			=> '',
			'bgcolor' 			=> '',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[event_main][event image="'.get_template_directory_uri().'/images/event-thumb1.jpg" event_date="20" event_month="July" title="Charity Marathon: Run for better life" event_time_icon="clock-o" event_time="08.00 - 16.00" event_location_icon="map-marker" event_location="Vallejo, California" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum" button="Read More" link="#"][/event][event image="'.get_template_directory_uri().'/images/event-thumb2.jpg" event_date="20" event_month="July" title="Charity Marathon: Run for better life" event_time_icon="clock-o" event_time="08.00 - 16.00" event_location_icon="map-marker" event_location="Vallejo, California" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum" button="Read More" link="#"][/event][event image="'.get_template_directory_uri().'/images/event-thumb3.jpg" event_date="20" event_month="July" title="Charity Marathon: Run for better life" event_time_icon="clock-o" event_time="08.00 - 16.00" event_location_icon="map-marker" event_location="Vallejo, California" content="Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum" button="Read More" link="#" class="last"][/event][/event_main]'),

		5 => array(
			'section_title'		=> 'Our Team',
			'menutitle'			=> '',
			'bgcolor' 			=> '#f7f7f7',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[ourteam show="4"]'
		),

		6 => array(
			'section_title'		=> 'Latest News',
			'menutitle'			=> '',
			'bgcolor' 			=> '',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[latestposts show="4"]'
			),

		7 => array(
			'section_title'		=> '',
			'menutitle'			=> 'testimonial-section',
			'bgcolor' 			=> '',
			'bgimage'			=> get_template_directory_uri().'/images/testimonial-bg.jpg',
			'class'				=> '',
			'content'			=> '[testimonials][readmore-link align="center" button="View All Our Team" links="#" target=""]'
		),

		8 => array(
			'section_title'		=> 'Our Partners',
			'menutitle'			=> 'client-section',
			'bgcolor' 			=> '',
			'bgimage'			=> '',
			'class'				=> '',
			'content'			=> '[client_logo_section][client_logo link="#" image="'.get_template_directory_uri().'/images/client-logo1.jpg"][/client_logo][client_logo link="#" image="'.get_template_directory_uri().'/images/client-logo2.jpg"][/client_logo][client_logo link="#" image="'.get_template_directory_uri().'/images/client-logo3.jpg"][/client_logo][client_logo link="#" image="'.get_template_directory_uri().'/images/client-logo4.jpg"][/client_logo][client_logo link="#" image="'.get_template_directory_uri().'/images/client-logo5.jpg" class="last"][/client_logo][/client_logo_section][readmore-link align="center" button="View All" links="#" target=""] '
		),

	);

	// Typography Body
	$typography_body = array(
		'size' => '13px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '400',
		'transform' => 'none',
		'color' => '#000000' );
		
	// Typography Logo
	$typography_logo = array(
		'size' => '35px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '300',
		'transform' => 'inherit',
		'color' => '#ffffff' );
	
	// Typography Logo Tag
	$typography_tagline = array(
		'size' => '14px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '400',
		'transform' => 'none',
		'color' => '#ffffff' );
		
	// Typography Navigation
	$typography_navigation = array(
		'size' => '15px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'none',
		'color' => '#a19389' );

	// Typography Slider Title
	$typography_slidertitle = array(
		'size' => '40px',
		'face' => 'Lato',
		'style' => 'italic',
		'weight' => '700',
		'transform' => 'inherit',
		'color' => '#ffffff' );

	// Typography Slider Description
	$typography_sliderdesc = array(
		'size' => '13px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '400',
		'transform' => 'inherit',
		'color' => '#ffffff' );
		
	// Section Title
	$typography_sectiontitle = array(
		'size' => '25px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'inherit',
		'color' => '#2b2b2b' );
		
	// Section Title
	$typography_footertitle = array(
		'size' => '15px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'uppercase',
		'color' => '#ffd800' );
		
	// Heading Title h1
	$typography_headingh1 = array(
		'size' => '30px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '400',
		'transform' => 'uppercase',
		'color' => '#282828' );
		
	// Heading Title h2
	$typography_headingh2 = array(
		'size' => '25px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'inherit',
		'color' => '#2b2b2b' );
		
	// Heading Title h3
	$typography_headingh3 = array(
		'size' => '20px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'uppercase',
		'color' => '#333333' );

	// Heading Title h4
	$typography_headingh4 = array(
		'size' => '17px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'uppercase',
		'color' => '#333333' );
		
	// Heading Title h5
	$typography_headingh5 = array(
		'size' => '15px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '700',
		'transform' => 'uppercase',
		'color' => '#000000' );
		
	// Heading Title h6
	$typography_headingh6 = array(
		'size' => '14px',
		'face' => 'Lato',
		'style' => 'normal',
		'weight' => '400',
		'transform' => 'inherit',
		'color' => '#000000' );


	//Basic Settings
	$options[] = array(
		'name' => __('Basic Settings', 'skt-trust'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Logo', 'skt-trust'),
		'desc' => __('Upload your main logo here', 'skt-trust'),
		'id' => 'logo',
		'std'	=> '',
		'type' => 'upload');

	$options[] = array(
		'name' => __('Logo Height', 'skt-trust'),
		'desc' => __('If your logo is large, you can increase this number to allow for more height.', 'skt-trust'),
		'id' => 'logoheight',
		'std' => '35',
		'type' => 'text');

// Latest News Section
	$options[] = array(
		'name' => __('Content Lenght', 'skt-trust'),		
		'desc' => __('Change content lenght for Latest Posts', 'skt-trust'),
		'id' => 'newscontentlength',
		'std' => '30',
		'type' => 'text');

	$options[] = array(
		'desc' => __('Blog post page change text content lenth', 'skt-trust'),
		'id' => 'blogpostpagecontent',
		'std' => '90',
		'type' => 'text');		
			
	$options[] = array(
		'desc' => __('Change content lenth for footer post', 'skt-trust'),
		'id' => 'footerpostconlength',
		'std' => '7',
		'type' => 'text');		
			
	$options[] = array(
		'desc' => __('Change blog post Read More button text (Read More).', 'skt-trust'),
		'id' => 'readmoretext',
		'std' => 'Read More',
		'type' => 'text');	

	$options[] = array(
		'name' => __('Custom CSS', 'skt-trust'),
		'desc' => __('Some Custom Styling for your site. Place any css codes here instead of the style.css file.', 'skt-trust'),
		'id' => 'style2',
		'std' => '',
		'type' => 'textarea');

// Body Typography	
	$options[] = array(
		'name' => __('Body Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for body text', 'skt-trust'),
		'id' => 'bodyfontface',
		'std'	=> $typography_body,
		'type' => 'typography');

	$options[] = array(
		'desc'	=> __('Select backgound color for Body','skt-trust'),
		'id' 	=> 'bodybgcolor',
		'std'   => '#ffffff',
		'type'	=> 'color');

	$options[] = array(	
		'name' => __('Header Top Color Setting', 'skt-trust'),
		'desc' => __('Select background color for header top', 'skt-trust'),
		'id' => 'headertopbgcolor',
		'std' => '#ffd800',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select text color for header top', 'skt-trust'),
		'id' => 'headertoptextcolor',
		'std' => '#282828',
		'type' => 'color');

	$options[] = array(	
		'name' => __('Header Background Color', 'skt-trust'),
		'desc' => __('Select background color for header', 'skt-trust'),
		'id' => 'headerbgcolor',
		'std' => '#26211d',
		'type' => 'color');

// Logo Typography	
	$options[] = array(
		'name' => __('Textual Logo Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for logo text', 'skt-trust'),
		'id' => 'logotype',
		'std'	=> $typography_logo,
		'type' => 'typography');

// Logo Tag Typography	
	$options[] = array(
		'name' => __('Textual Logo Tag Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for logo tag text', 'skt-trust'),
		'id' => 'tagtype',
		'std'	=> $typography_tagline,
		'type' => 'typography');


// Header Navigation
	$options[] = array(
		'name' => __('Navigation Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for navigation', 'skt-trust'),
		'id' => 'navtype',
		'std'	=> $typography_navigation,
		'type' => 'typography');

	$options[] = array(
		'desc' => __('Select hover color for navigation', 'skt-trust'),
		'id' => 'navigationhovercolor',
		'std' => '#282828',
		'type' => 'color');	

	$options[] = array(
		'desc' => __('Select background hover/active color for navigation', 'skt-trust'),
		'id' => 'navigationhoverbgcolor',
		'std' => '#ffd800',
		'type' => 'color');	

	$options[] = array(
		'desc' => __('Select background color for dropdown navigation', 'skt-trust'),
		'id' => 'navigationdrbgcolor',
		'std' => '#26211d',
		'type' => 'color');	

	$options[] = array(
		'desc' => __('Select border bottom color for dropdown navigation', 'skt-trust'),
		'id' => 'navigationdrbtbrcolor',
		'std' => '#848484',
		'type' => 'color');	

// Slider Title Typography	
	$options[] = array(
		'name' => __('Slider Title Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for slider title', 'skt-trust'),
		'id' => 'slidertitletype',
		'std'	=> $typography_slidertitle,
		'type' => 'typography');

// Slider Description Typography	
	$options[] = array(
		'name' => __('Slider Description Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for logo tag text', 'skt-trust'),
		'id' => 'sliderdesctype',
		'std'	=> $typography_sliderdesc,
		'type' => 'typography');

	$options[] = array(	
		'desc' => __('Select color for Slider Read More', 'skt-trust'),
		'id' => 'sliderbutcolor',
		'std' => '#ffffff',
		'type' => 'color');
	$options[] = array(	
		'desc' => __('Select border color for Slider Read More button', 'skt-trust'),
		'id' => 'sliderbutbrcolor',
		'std' => '#ffffff',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select hover color for Slider Read More', 'skt-trust'),
		'id' => 'sliderbuttexthvcolor',
		'std' => '#ffffff',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select background hover color for Slider Read More', 'skt-trust'),
		'id' => 'sliderbutbghvcolor',
		'std' => '#ffd800',
		'type' => 'color');

// Slider controls colors		
	$options[] = array(
		'name' => __('Slider Control Colors ', 'skt-trust'),
		'desc' => __('Select background color for slider navigation', 'skt-trust'),
		'id' => 'sldnavbg',
		'std' => '#ffffff',
		'type' => 'color');
	$options[] = array(
		'desc' => __('Select background color for slider navigation hover', 'skt-trust'),
		'id' => 'sldnavhvbg',
		'std' => '#ffd800',
		'type' => 'color');
	$options[] = array(
		'desc' => __('Select background color for slider pager', 'skt-trust'),
		'id' => 'sldpagebg',
		'std' => '#e7e7e7',
		'type' => 'color');
	$options[] = array(
		'desc' => __('Select background color for slider pager active', 'skt-trust'),
		'id' => 'sldpagehvbg',
		'std' => '#ffd800',
		'type' => 'color');	

// Section Title Typography	
	$options[] = array(
		'name' => __('Section title Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for Section title', 'skt-trust'),
		'id' => 'sectiontitletype',
		'std'	=> $typography_sectiontitle,
		'type' => 'typography');

// Why Choose
	$options[] = array(
		'name' => __('Why Choose Us Color Setting', 'skt-trust'),
		'desc' => __('Select icon  color for why choose us', 'character'),
		'id' => 'whychooseiconcolor',
		'std' => '#222222',
		'type' => 'color');		

	$options[] = array(
		'desc' => __('Select icon hover and icon border color for why choose us', 'character'),
		'id' => 'whychoosetextcolor',
		'std' => '#ffd800',
		'type' => 'color');		

// Why Events
	$options[] = array(
		'name' => __('Events Color Setting', 'skt-trust'),
		'desc' => __('Select text color for events', 'character'),
		'id' => 'eventtextcolor',
		'std' => '#949393',
		'type' => 'color');		

	$options[] = array(
		'desc' => __('Select text color for events date', 'character'),
		'id' => 'eventdttextcolor',
		'std' => '#383939',
		'type' => 'color');		

	$options[] = array(
		'desc' => __('Select background color for events date', 'character'),
		'id' => 'eventdtbgcolor',
		'std' => '#ffd800',
		'type' => 'color');		

// Our Causes
	$options[] = array(
		'name' => __('Our Causes Color Setting', 'skt-trust'),
		'desc' => __('Select color for Our Causes content', 'character'),
		'id' => 'causestextcolor',
		'std' => '#858484',
		'type' => 'color');		

	$options[] = array(
		'desc' => __('Select downarrow color for Our Causes content percent', 'character'),
		'id' => 'causespercentarrow',
		'std' => '#ffd800',
		'type' => 'color');		

// Testimonial setting
	$options[] = array(
		'name' => __('Testimonial Setting', 'skt-trust'),
		'desc' => __('Manage Testimonials Rotating Speeds', 'character'),
		'id' => 'testirotatespeed',
		'std' => '1000',
		'type' => 'text');		

	$options[] = array(
		'desc' => __('Testimonials Auto Rotate True/False', 'character'),
		'id' => 'testimonialsautorotate',
		'std' => 'true',
		'type' => 'select',
		'options' => array('true'=>'True', 'false'=>'False'));

	$options[] = array(
		'desc' => __('Select color for testimonial', 'skt-trust'),
		'id' => 'testimonialbgcolor',
		'std' => '#f6f4f4',
		'type' => 'color',);

//Team Code
	$options[] = array(
		'name' => __('Team section', 'skt-trust'),
		'desc' => __('Change content lenght for Our Team ', 'skt-trust'),
		'id' => 'contentlenghtourteam',
		'std' => '22',
		'type' => 'text');			

	$options[] = array(
		'desc' => __('Our Team View All Button text', 'skt-trust'),
		'id' => 'staffbuttontext',
		'std' => 'View All Team Members',
		'type' => 'text');	

	$options[] = array(
		'desc' => __('Paste url here for View All Team Member', 'skt-trust'),
		'id' => 'staffbuttonurl',
		'std' => '#',
		'type' => 'text');	

//Client Logo
	$options[] = array(
		'name' => __('Client Logo Section', 'skt-trust'),
		'desc' => __('Select background color for client logo', 'skt-trust'),
		'id' => 'clientlogobgcolor',
		'std' => '#f7f7f7',
		'type' => 'color');			

	$options[] = array(
		'desc' => __('Select boroder color for client logo', 'skt-trust'),
		'id' => 'clientlogobrcolor',
		'std' => '#ffd800',
		'type' => 'color');			

//Gallery			
	$options[] = array(
		'name' => __('Gallery background & color setting', 'skt-trust'),
		'desc' => __('Select navigation color', 'skt-trust'),
		'id' => 'gallerynavigationcolor',
		'std' => '#ffffff',
		'type' => 'color');	
	$options[] = array(
		'desc' => __('Select navigation active background color', 'skt-trust'),
		'id' => 'gallerynavbgcolor',
		'std' => '#ffd800',
		'type' => 'color');	
	$options[] = array(
		'desc' => __('Select navigation active color', 'skt-trust'),
		'id' => 'gallerynavtxtcolor',
		'std' => '#282828',
		'type' => 'color');	

	$options[] = array(
		'desc' => __('Select background hover color for gallery', 'skt-trust'),
		'id' => 'gallerybgcolor',
		'std' => '#ffd800',
		'type' => 'color');	

	$options[] = array(
		'desc' => __('Select Show All Text for filter gallery', 'skt-trust'),
		'id' => 'showalltextfilgr',
		'std' => 'Show All',
		'type' => 'text');	

	// All anchor tag link/hover 
	$options[] = array(	
		'name' => __('Site All Anchor tag link/hover Colors', 'skt-trust'),	
		'desc' => __('Select color for site anchar tag link', 'skt-trust'),
		'id' => 'anchortagcolor',
		'std' => '#ffd800',
		'type' => 'color');
	$options[] = array(	
		'desc' => __('Select color for site anchor tag link hover', 'skt-trust'),
		'id' => 'anchortagcolorhover',
		'std' => '#5b5a5a',
		'type' => 'color');


	// Default Buttons		
	$options[] = array(
		'name' => __('Read More Button Colors', 'skt-trust'),		
		'desc' => __('Select text color for button', 'skt-trust'),
		'id' => 'btntextcolor',
		'std' => '#ffffff',
		'type' => 'color');
	$options[] = array(
		'desc' => __('Select background color button', 'skt-trust'),
		'id' => 'btnbgcolor',
		'std' => '#383939',
		'type' => 'color');
	$options[] = array(
		'desc' => __('Select hover text color for button', 'skt-trust'),
		'id' => 'btntxthovercolor',
		'std' => '#383939',
		'type' => 'color');		
	$options[] = array(
		'desc' => __('Select background hover color button on hover', 'skt-trust'),
		'id' => 'btnhoverbgcolor',
		'std' => '#ffd800',
		'type' => 'color');	

// Default Social Icon
	$options[] = array(
		'name' => __('Social icon color setting', 'skt-trust'),
		'desc' => __('Select color for social icon', 'skt-trust'),
		'id' => 'socialfontcolor',
		'std' => '#ffffff',
		'type' => 'color');		
	$options[] = array(
		'desc' => __('Select background hover color for social icon', 'skt-trust'),
		'id' => 'socialfonthvcolor',
		'std' => '#ffffff',
		'type' => 'color');				
	$options[] = array(
		'desc' => __('Select background color for social icon', 'skt-trust'),
		'id' => 'socialbgcolor',
		'std' => '#545556',
		'type' => 'color');				
	$options[] = array(
		'desc' => __('Select background hover color for social icon', 'skt-trust'),
		'id' => 'socialbghvcolor',
		'std' => '#ffd800',
		'type' => 'color');				
	
// Footer Settings
 	$options[] = array(
		'name' => __('Footer title Typography', 'skt-trust'),
		'desc' => __('Change font size/family/style/weight/transform/color for Section title', 'skt-trust'),
		'id' => 'footertitletype',
		'std'	=> $typography_footertitle,
		'type' => 'typography');

	$options[] = array(	
		'name' => __('Background color for footer and copyright', 'skt-trust'),	
		'desc' => __('Select background color for footer', 'skt-trust'),
		'id' => 'footerwpbgcolor',
		'std' => '#26211d',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select Border Top Color for Footer', 'skt-trust'),
		'id' => 'footertopbrcolor',
		'std' => '#ffd800',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select color for footer', 'skt-trust'),
		'id' => 'footerwpcolor',
		'std' => '#ffffff',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select text color for copyright', 'skt-trust'),
		'id' => 'copyrighttxcolor',
		'std' => '#ffffff',
		'type' => 'color');

	$options[] = array(	
		'desc' => __('Select background color for copyright', 'skt-trust'),
		'id' => 'copyrightbgcolor',
		'std' => '#1b1715',
		'type' => 'color');


// Sidebar option
	$options[] = array(
		'name' => __('Sidebar for background/border color', 'skt-trust'),	
		'desc' => __('Select background color for sidebar widget title', 'skt-trust'),
		'id' => 'widgettitlebgcolor',
		'std' => '#ffd800 ',
		'type' => 'color');	
	
	$options[] = array(
		'desc' => __('Select color for sidebar title ', 'skt-trust'),
		'id' => 'widgettitlecolor',
		'std' => '#ffffff',
		'type' => 'color');	
			
	$options[] = array(
		'desc' => __('Select background color for sidebar widget', 'skt-trust'),
		'id' => 'sidebarbgcolor',
		'std' => '#f9f9f9',
		'type' => 'color');	
				
	$options[] = array(			
		'desc' => __('Select text color for sidebar li a', 'skt-trust'),
		'id' => 'sidebarlnkcolor',
		'std' => '#000000',
		'type' => 'color');		

	$options[] = array(			
		'desc' => __('Select border color for sidebar li a', 'skt-trust'),
		'id' => 'sidebarliaborder',
		'std' => '#000000',
		'type' => 'color');		

	$options[] = array(
		'desc' => __('Select background color for search form', 'skt-trust'),
		'id' => 'searchbgcolor',
		'std' => '#f9f9f9',
		'type' => 'color');	

// Input field color setting
	$options[] = array(
		'name' => __('Select All Input field background & text color', 'skt-trust'),
		'desc' => __('Select background color for input field and textarea', 'skt-trust'),
		'id' => 'inputbgcolor',
		'std' => '#ffffff',
		'type' => 'color');	
	$options[] = array(
		'desc' => __('Select color for input field and textarea', 'skt-trust'),
		'id' => 'inputtextbgcolor',
		'std' => '#2b2b2b',
		'type' => 'color');	
	$options[] = array(
		'desc' => __('Select border color for input field and textarea', 'skt-trust'),
		'id' => 'inputtextborcolor',
		'std' => '#e7e7e7',
		'type' => 'color');	
		
// Site title heading Settings
 	$options[] = array(
		'name' => __('Heading Heading h1, h2, h3, h4, h5, h6 Typography', 'skt-trust'),
		'desc' => __('Change Heading h1 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh1type',
		'std'	=> $typography_headingh1,
		'type' => 'typography');
		
	$options[] = array(
		'desc' => __('Change Heading h2 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh2type',
		'std'	=> $typography_headingh2,
		'type' => 'typography');
		
	$options[] = array(
		'desc' => __('Change Heading h3 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh3type',
		'std'	=> $typography_headingh3,
		'type' => 'typography');

	$options[] = array(
		'desc' => __('Change Heading h4 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh4type',
		'std'	=> $typography_headingh4,
		'type' => 'typography');

	$options[] = array(
		'desc' => __('Change Heading h5 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh5type',
		'std'	=> $typography_headingh5,
		'type' => 'typography');

	$options[] = array(
		'desc' => __('Change Heading h6 tag font size/family/style/weight/transform/color for heading title', 'skt-trust'),
		'id' => 'headingh6type',
		'std'	=> $typography_headingh6,
		'type' => 'typography');
		
	$options[] = array(
		'name' => __('Blog Single Layout', 'skt-trust'),
		'desc' => __('Select layout. eg:Boxed, Wide', 'skt-trust'),
		'id' => 'singlelayout',
		'type' => 'select',
		'std' => 'singleright',
		'options' => array('singleright'=>'Blog Single Right Sidebar', 'singleleft'=>'Blog Single Left Sidebar', 'sitefull'=>'Blog Single Full Width', 'nosidebar'=>'Blog Single No Sidebar') );
		
	$options[] = array(
		'name' => __('Woocommerce Page Layout', 'skt-trust'),
		'desc' => __('Select layout. eg:Boxed, Wide', 'skt-trust'),
		'id' => 'woocomercelayout',
		'type' => 'select',
		'std' => 'woocomerceright',
		'options' => array('woocomerceright'=>'Woocomerce Right Sidebar', 'woocomerceleft'=>'Woocomerce Left Sidebar', 'woocomercesitefull'=>'Woocomerce Full Width') );			
		
	$options[] = array(			
		'name' => __('Woocommerce Price filter color setting', 'skt-trust'),
		'desc' => __('Select color for Price slider background', 'skt-trust'),
		'id' => 'pricesliderbg',
		'std' => '#cccccc',
		'type' => 'color');	

	$options[] = array(			
		'desc' => __('Select color for Price slider handle background', 'skt-trust'),
		'id' => 'pricehandlebg',
		'std' => '#ffd800',
		'type' => 'color');	

	$options[] = array(			
		'desc' => __('Select color for Price slider Range background', 'skt-trust'),
		'id' => 'pricerangebg',
		'std' => '#515151',
		'type' => 'color');


	//Layout Settings
	$options[] = array(
		'name' => __('Sections', 'skt-trust'),
		'type' => 'heading');

// Sections 	
	$options[] = array(
		'name' => __('Show/Hide Home Four Pages', 'skt-trust'),
		'desc' => __('Select show/hide option for home four column pages', 'skt-trust'),
		'id' => 'pagesboxshowhide',
		'std' => 'Show',
		'type' => 'select',
		'options' => array('show'=>'Show', 'hide'=>'Hide') );	

	$options[] = array(
		'name' => __('Select First Section Title', 'skt-trust'),
		'desc' => __('Select title for first section', 'skt-trust'),
		'id' => 'sectionfrtitle',
		'std' => '<span>Help people by donating little.</span> See our causes',
		'type' => 'textarea');

// Three Pages 	
	$options[] = array(
		'name' => __('Home Four Pages', 'skt-trust'),
		'desc' => __('Select home four column pages', 'skt-trust'),
		'id' => 'box1',
		'type' => 'select',
		'options' => $options_pages,
	);
	$options[] = array(
		'desc' => __('Upload Image here for box 1', 'skt-trust'),
		'id' => 'boximage1',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(	
		'desc'	=> __('Home Page Box 2','skt-trust'),
		'id' 	=> 'box2',
		'type'	=> 'select',
		'options' => $options_pages,
	);

	$options[] = array(
		'desc' => __('Upload Image here for box 2', 'skt-trust'),
		'id' => 'boximage2',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(	
		'desc'	=> __('Home Page Box 3','skt-trust'),
		'id' 	=> 'box3',
		'type'	=> 'select',
		'options' => $options_pages,
	);	
	
	$options[] = array(
		'desc' => __('Upload Image here for box 3', 'skt-trust'),
		'id' => 'boximage3',
		'std' => '',
		'type' => 'upload');
	
	$options[] = array(	
		'desc'	=> __('Home Page Box 4','skt-trust'),
		'id' 	=> 'box4',
		'type'	=> 'select',
		'options' => $options_pages,
	);	
	
	$options[] = array(
		'desc' => __('Upload Image here for box 4', 'skt-trust'),
		'id' => 'boximage4',
		'std' => '',
		'type' => 'upload');
	
		$options[] = array(	
		'desc'	=> __('Home Page Box 5','skt-trust'),
		'id' 	=> 'box5',
		'type'	=> 'select',
		'options' => $options_pages,
	);	
	
	$options[] = array(
		'desc' => __('Upload Image here for box 5', 'skt-trust'),
		'id' => 'boximage5',
		'std' => '',
		'type' => 'upload');

	$options[] = array(	
		'desc'	=> __('Home Page Box 6','skt-trust'),
		'id' 	=> 'box6',
		'type'	=> 'select',
		'options' => $options_pages,
	);	
	
	$options[] = array(
		'desc' => __('Upload Image here for box 6', 'skt-trust'),
		'id' => 'boximage6',
		'std' => '',
		'type' => 'upload');

	$options[] = array(
		'name' => __("Select Content Lenght of Home Page Top Boxes", 'skt-trust'),
		'desc' => __("Select Content lenght for home page top boxes", 'skt-trust'),
		'id' => 'topboxescontentlength',
		'std' => '16',
		'type' => 'text');	

	$options[] = array(
		'name' => __("Select Read More for Home Page Boxes", 'skt-trust'),
		'desc' => __("Select read more button text for home page top boxes", 'skt-trust'),
		'id' => 'readmorebtntexttpbx',
		'std' => 'Donate Now',
		'type' => 'text');	

// Sections 	
	$options[] = array(
		'name' => __('Number of Sections', 'skt-trust'),
		'desc' => __('Select number of sections', 'skt-trust'),
		'id' => 'numsection',
		'type' => 'select',
		'std' => '8',
		'options' => array_combine(range(1,30), range(1,30)) );
	$numsecs = of_get_option( 'numsection', 7 );

	for( $n=1; $n<=$numsecs; $n++){
		$options[] = array(
			'desc' => __("<h3>Section ".$n."</h3>", 'skt-trust'),
			'class' => 'toggle_title',
			'type' => 'info');	
	
		$options[] = array(
			'name' => __('Section Title', 'skt-trust'),
			'id' => 'sectiontitle'.$n,
			'std' => ( ( isset($section_text[$n]['section_title']) ) ? $section_text[$n]['section_title'] : '' ),
			'type' => 'text');

		$options[] = array(
			'name' => __('Section ID', 'skt-trust'),
			'desc'	=> __('Enter your section ID here. SECTION ID MUST BE IN SMALL LETTERS ONLY AND DO NOT ADD SPACE OR SYMBOL.'),
			'id' => 'menutitle'.$n,
			'std' => ( ( isset($section_text[$n]['menutitle']) ) ? $section_text[$n]['menutitle'] : '' ),
			'type' => 'text');

		$options[] = array(
			'name' => __('Section Background Color', 'skt-trust'),
			'desc' => __('Select background color for section', 'skt-trust'),
			'id' => 'sectionbgcolor'.$n,
			'std' => ( ( isset($section_text[$n]['bgcolor']) ) ? $section_text[$n]['bgcolor'] : '' ),
			'type' => 'color');
			
		$options[] = array(
			'name' => __('Background Image', 'skt-trust'),
			'id' => 'sectionbgimage'.$n,
			'class' => '',
			'std' => ( ( isset($section_text[$n]['bgimage']) ) ? $section_text[$n]['bgimage'] : '' ),
			'type' => 'upload');

		$options[] = array(
			'name' => __('Section CSS Class', 'skt-trust'),
			'desc' => __('Set class for this section.', 'skt-trust'),
			'id' => 'sectionclass'.$n,
			'std' => ( ( isset($section_text[$n]['class']) ) ? $section_text[$n]['class'] : '' ),
			'type' => 'text');
			
		$options[] = array(
			'name'	=> __('Hide Section', 'skt-trust'),
			'desc'	=> __('Check to hide this section', 'skt-trust'),
			'id'	=> 'hidesec'.$n,
			'type'	=> 'checkbox',
			'std'	=> '');

		$options[] = array(
			'name' => __('Section Content', 'skt-trust'),
			'id' => 'sectioncontent'.$n,
			'std' => ( ( isset($section_text[$n]['content']) ) ? $section_text[$n]['content'] : '' ),
			'type' => 'editor');
	}

	//SLIDER SETTINGS
	$options[] = array(
		'name' => __('Homepage Slider', 'skt-trust'),
		'type' => 'heading');
		
	$options[] = array(
		'name' => __('Inner Page Banner', 'skt-trust'),
		'desc' => __('Upload inner page banner for site', 'skt-trust'),
		'id' => 'innerpagebanner',
		'class' => '',
		'std'	=> get_template_directory_uri()."/images/default-banner.jpg",
		'type' => 'upload');	
		
	$options[] = array(
		'name' => __('Slider Effects and Timing', 'skt-trust'),
		'desc' => __('Select slider effect.','skt-trust'),
		'id' => 'slideefect',
		'std' => 'random',
		'type' => 'select',
		'options' => array('random'=>'Random', 'fade'=>'Fade', 'fold'=>'Fold', 'sliceDown'=>'Slide Down', 'sliceDownLeft'=>'Slide Down Left', 'sliceUp'=>'Slice Up', 'sliceUpLeft'=>'Slice Up Left', 'sliceUpDown'=>'Slice Up Down', 'sliceUpDownLeft'=>'Slice Up Down Left', 'slideInRight'=>'SlideIn Right', 'slideInLeft'=>'SlideIn Left', 'boxRandom'=>'Box Random', 'boxRain'=>'Box Rain', 'boxRainReverse'=>'Box Rain Reverse', 'boxRainGrow'=>'Box Rain Grow', 'boxRainGrowReverse'=>'Box Rain Grow Reverse' ));
		
	$options[] = array(
		'desc' => __('Animation speed should be multiple of 100.', 'skt-trust'),
		'id' => 'slideanim',
		'std' => 500,
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Add slide pause time.', 'skt-trust'),
		'id' => 'slidepause',
		'std' => 5000,
		'type' => 'text');
		
	$options[] = array(
		'name' => __('Slide Controllers', 'skt-trust'),
		'desc' => __('Hide/Show Direction Naviagtion of slider.','skt-trust'),
		'id' => 'slidenav',
		'std' => 'false',
		'type' => 'select',
		'options' => array('true'=>'Show', 'false'=>'Hide'));
		
	$options[] = array(
		'desc' => __('Hide/Show pager of slider.','skt-trust'),
		'id' => 'slidepage',
		'std' => 'false',
		'type' => 'select',
		'options' => array('true'=>'Show', 'false'=>'Hide'));
		
	$options[] = array(
		'desc' => __('Pause Slide on Hover.','skt-trust'),
		'id' => 'slidepausehover',
		'std' => 'false',
		'type' => 'select',
		'options' => array('true'=>'Yes', 'false'=>'No'));
		
		
	$options[] = array(
		'name' => __('Use any slider shortcode', 'skt-trust'),
		'desc' => __('you add any slider plugin shortcode here', 'skt-trust'),
		'id' => 'slidershortcode',
		'std' => '',
		'type' => 'textarea');
		
		
	$options[] = array(
		'name' => __('Slider Image 1', 'skt-trust'),
		'desc' => __('First Slide', 'skt-trust'),
		'id' => 'slide1',
		'class' => '',
		'std' => get_template_directory_uri()."/images/slides/slider1.jpg",
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 1', 'skt-trust'),
		'id' => 'slidetitle1',
		'std' => 'Donate & Help',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc1',
		'std' => 'Donec id laoreet orci. Quisque lacus nulla, dapibus sed nisi vel, faucibus sodales odio. Donec eget euismod mi. Phasellus malesuada tristique eros ac volutpat. Aenean venenatis, risus id fermentum facilisis, purus metus mattis tortor, quis convallis velit lorem sit amet sapien',
		'type' => 'textarea');

	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton1',
		'std' => 'Buy This Theme',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl1',
		'std' => '#',
		'type' => 'text');		
	
	$options[] = array(
		'name' => __('Slider Image 2', 'skt-trust'),
		'desc' => __('Second Slide', 'skt-trust'),
		'class' => '',
		'id' => 'slide2',
		'std' => get_template_directory_uri()."/images/slides/slider2.jpg",
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 2', 'skt-trust'),
		'id' => 'slidetitle2',
		'std' => 'Slider Title',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc2',
		'std' => 'Donec id laoreet orci. Quisque lacus nulla, dapibus sed nisi vel, faucibus sodales odio. Donec eget euismod mi. Phasellus malesuada tristique eros ac volutpat. Aenean venenatis, risus id fermentum facilisis, purus metus mattis tortor, quis convallis velit lorem sit amet sapien',
		'type' => 'textarea');	
		
	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton2',
		'std' => 'Buy This Theme',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl2',
		'std' => '#',
		'type' => 'text');		
	
	$options[] = array(
		'name' => __('Slider Image 3', 'skt-trust'),
		'desc' => __('Third Slide', 'skt-trust'),
		'id' => 'slide3',
		'std' => get_template_directory_uri()."/images/slides/slider3.jpg",
		'type' => 'upload');
		
$options[] = array(
		'desc' => __('Title 3', 'skt-trust'),
		'id' => 'slidetitle3',
		'std' => 'Slier Title 3',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc3',
		'std' => 'Donec id laoreet orci. Quisque lacus nulla, dapibus sed nisi vel, faucibus sodales odio. Donec eget euismod mi. Phasellus malesuada tristique eros ac volutpat. Aenean venenatis, risus id fermentum facilisis, purus metus mattis tortor, quis convallis velit lorem sit amet sapien',
		'type' => 'textarea');	
		
	$options[] = array(
		'desc' => __('Slide Read More Button text', 'skt-trust'),
		'id' => 'slidebutton3',
		'std' => 'Buy This Theme',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl3',
		'std' => '#',
		'type' => 'text');		
			
	$options[] = array(
		'name' => __('Slider Image 4', 'skt-trust'),
		'desc' => __('Fourth Slide', 'skt-trust'),
		'id' => 'slide4',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 4', 'skt-trust'),
		'id' => 'slidetitle4',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc4',
		'std' => '',
		'type' => 'textarea');	

	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton4',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl4',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'name' => __('Slider Image 5', 'skt-trust'),
		'desc' => __('Fifth Slide', 'skt-trust'),
		'id' => 'slide5',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 5', 'skt-trust'),
		'id' => 'slidetitle5',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc5',
		'std' => '',
		'type' => 'textarea');	

	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton5',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl5',
		'std' => '',
		'type' => 'text');		
		
	$options[] = array(
		'name' => __('Slider Image 6', 'skt-trust'),
		'desc' => __('Sixth Slide', 'skt-trust'),
		'id' => 'slide6',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 6', 'skt-trust'),
		'id' => 'slidetitle6',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc6',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton6',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl6',
		'std' => '',
		'type' => 'text');		
		
	$options[] = array(
		'name' => __('Slider Image 7', 'skt-trust'),
		'desc' => __('Seventh Slide', 'skt-trust'),
		'id' => 'slide7',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 7', 'skt-trust'),
		'id' => 'slidetitle7',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc7',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton7',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl7',
		'std' => '',
		'type' => 'text');		
		
	$options[] = array(
		'name' => __('Slider Image 8', 'skt-trust'),
		'desc' => __('Eighth Slide', 'skt-trust'),
		'id' => 'slide8',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 8', 'skt-trust'),
		'id' => 'slidetitle8',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc8',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton8',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl8',
		'std' => '',
		'type' => 'text');		
		
	$options[] = array(
		'name' => __('Slider Image 9', 'skt-trust'),
		'desc' => __('Ninth Slide', 'skt-trust'),
		'id' => 'slide9',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 9', 'skt-trust'),
		'id' => 'slidetitle9',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc9',
		'std' => '',
		'type' => 'textarea');

	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton9',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl9',
		'std' => '',
		'type' => 'text');		
		
	$options[] = array(
		'name' => __('Slider Image 10', 'skt-trust'),
		'desc' => __('Tenth Slide', 'skt-trust'),
		'id' => 'slide10',
		'class' => '',
		'std' => '',
		'type' => 'upload');
		
	$options[] = array(
		'desc' => __('Title 10', 'skt-trust'),
		'id' => 'slidetitle10',
		'std' => '',
		'type' => 'text');
		
	$options[] = array(
		'desc' => __('Description or Tagline', 'skt-trust'),
		'id' => 'slidedesc10',
		'std' => '',
		'type' => 'textarea');
		
	$options[] = array(
		'desc' => __('Slide Read More Button Text', 'skt-trust'),
		'id' => 'slidebutton10',
		'std' => '',
		'type' => 'text');		
	
	$options[] = array(
		'desc' => __('Slide Read More Button Url', 'skt-trust'),
		'id' => 'slideurl10',
		'std' => '',
		'type' => 'text');		

	//Header SETTINGS
	$options[] = array(
		'name' => __('Header', 'skt-trust'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Header Mobile menu text', 'skt-trust'),
		'desc' => 'Change Menu Text of header in Mobile.',
		'id' => 'headermobiletext',
		'std' => 'Menu',
		'type' => 'text');	
		
	$options[] = array(
		'name' => __('Header Top Left Information', 'skt-trust'),
		'desc' => 'Can change header left Phone Number',
		'id' => 'headerphonetextarea',
		'std' => '<a href="#"><i class="fa fa-phone"></i> 12 8888 66666</a>',
		'type' => 'textarea');	
		
	$options[] = array(
		'desc' => 'Can change header top left Email',
		'id' => 'headertopemail',
		'std' => '<a href="mailto:info@sitename.com"><i class="fa fa-envelope"></i> info@sitename.com</a>',
		'type' => 'textarea');	

	$options[] = array(
		'name' => __('Header Top Right Information', 'skt-trust'),
		'desc' => 'Can change header top right social icons',
		'id' => 'headertoprighttexarea',
		'std' => ' [social_area][social icon="facebook" link="#"][social icon="twitter" link="#"][social icon="linkedin" link="#"][social icon="google-plus" link="#"][social icon="youtube" link="#"][/social_area]',
		'type' => 'textarea');	
		
	//Footer SETTINGS
	$options[] = array(
		'name' => __('Footer', 'skt-trust'),
		'type' => 'heading');

	$options[] = array(
		'name' => __('Footer Layout', 'skt-trust'),
		'desc' => __('footer Select layout. eg:Boxed, 1, 2, 3 and 4', 'skt-trust'),
		'id' => 'footerlayout',
		'type' => 'select',
		'std' => 'fourcolumn',
		'options' => array('onecolumn'=>'Footer 1 column', 
		'twocolumn'=>'Footer 2 column', 'threecolumn'=>'Footer 3 column', 'fourcolumn'=>'Footer 4 column', ) );	
		
	$options[] = array(
		'name' => __('Footer About Us Title', 'skt-trust'),
		'desc' => __('About Us title for footer', 'skt-trust'),
		'id' => 'footerabouttexttitle',
		'std' => 'About Us',
		'type' => 'text');

	$options[] = array(
		'name' => __('Footer Latest Post Title', 'skt-trust'),
		'desc' => __('Latest post title for footer.', 'skt-trust'),
		'id' => 'footerrcposttitle',
		'std' => 'Latest Posts',
		'type' => 'text');	

	$options[] = array(
		'name' => __('Footer Latest Tweets Title', 'skt-trust'),
		'desc' => __('Latest Twitts Title for Footer', 'skt-trust'),
		'id' => 'footertwitttitle',
		'std' => 'Latest Twitts',
		'type' => 'text');

	$options[] = array(
		'name' => __('Footer Address Title', 'skt-trust'),
		'desc' => __('Add contact title here', 'skt-trust'),
		'id' => 'contacttitle',
		'std' => 'Contact Info',
		'type' => 'text');	

	$options[] = array(	
		'name' => __('Footer Address', 'skt-trust'),
		'desc' => __('Add company address1 here.', 'skt-trust'),
		'id' => 'address',
		'std' => '123 Bridge Street,<br />
New York, NY 666555',
		'type' => 'textarea');
		
	$options[] = array(	
		'desc' => __('Add company address2 here.', 'skt-trust'),
		'id' => 'address2',
		'std' => '',
		'type' => 'text');	
		
	$options[] = array(		
		'desc' => __('Add phone number here.', 'skt-trust'),
		'id' => 'phone',
		'std' => 'Phone: 1.800.555.6789',
		'type' => 'textarea');
		
	$options[] = array(		
		'desc' => __('Add phone number 2 here.', 'skt-trust'),
		'id' => 'phone2',
		'std' => '',
		'type' => 'text');

	$options[] = array(
		'desc' => __('Add email address here.', 'skt-trust'),
		'id' => 'email',
		'std' => 'Email: <a href="mailto:support@sitename.com">support@sitename.com</a>',
		'type' => 'textarea');
		
	$options[] = array(
		'desc' => __('Add company url here with http://.', 'skt-trust'),
		'id' => 'weblink',
		'std' => 'website: <a href="https://sktthemes.net/" target="_blank">sktthemes.net</a>',
		'type' => 'textarea');

	$options[] = array(
		'name' => __('Footer social Media'),
		'desc' => __('Add here footr social media.', 'skt-trust'),
		'id' => 'footersocialmedia',
		'std' => '[social_area]
    [social icon="facebook" link="#"]
    [social icon="google-plus" link="#"]
    [social icon="twitter" link="#"]
    [social icon="linkedin" link="#"]
    [social icon="pinterest" link="#"]
    [social icon="youtube" link="#"]
[/social_area]',
		'type' => 'textarea');
			
	$options[] = array(
		'name' => __('Footer Twitter Shorcode', 'skt-trust'),
		'desc' => __('Add here twitts shortcode for footer.', 'skt-trust'),
		'id' => 'footertwittercode',
		'std' => '<a class="twitter-timeline" data-chrome="nofooter noheader noborders noscroll noscrollbar transparent" data-tweet-limit="1" data-link-color="#ffd800"  data-theme="dark" data-dnt="true" href="https://twitter.com/sktthemes"  data-widget-id="353086898853531648">Tweets by @sktthemes</a>',
		'type' => 'editor');	

	$options[] = array(
		'name' => __('Footer About Us Content', 'skt-trust'),
		'desc' => __('Add here subcribe content for footer.', 'skt-trust'),
		'id' => 'footeraboutcontent',
		'std' => '<p>Sed suscipit mauris nec mauris vulputate, a posuere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula.</p>
<p>Nam metus lorem, hendrerit quis ante eget, lobortis elementum neque. Aliquam in ullamcorper quam. Integer euismod ligula in mauris vehicula imperdiet. Cras in convallis ipsum. Phasellus turpis, aliquet non quis, tristique tempus turpis.</p>',
		'type' => 'textarea');	

	$options[] = array(
		'name' => __('Contact Map', 'skt-trust'),
		'desc' => __('Add contact map ifram form: <strong>https://www.google.co.in/maps</strong>', 'skt-trust'),
		'id' => 'contactmap',
		'std' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d317715.71193215286!2d-0.38178504195904805!3d51.52873519673248!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d8a00baf21de75%3A0x52963a5addd52a99!2sLondon%2C+UK!5e0!3m2!1sen!2sin!4v1464759647206" width="100%" height="360" frameborder="0" allowfullscreen></iframe>',
		'type' => 'editor');

	$options[] = array(
		'name' => __('Footer Copyright', 'skt-trust'),
		'desc' => __('Copyright Text for your site.', 'skt-trust'),
		'id' => 'copytext',
		'std' => 'Copyright 2016 <a href="'.esc_url('https://www.sktthemes.net/').'" target="_blank">SKT Themes</a>  All Rights Reserved',
		'type' => 'textarea');
		
	$options[] = array(
		'desc' => __('Footer Text Link', 'skt-trust'),
		'id' => 'ftlink',
		'std' => '',
		'type' => 'textarea',);

	//Short codes
	$options[] = array(
		'name' => __('Short Codes', 'skt-trust'),
		'type' => 'heading');	

	$options[] = array(
		'name' => __('Photo Gallery', 'skt-trust'),
		'desc' => __('[photogallery filter="true"]', 'skt-trust'),
		'type' => 'info');		

	$options[] = array(
		'name' => __('Testimonials', 'skt-trust'),
		'desc' => __('[testimonials]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('View All Testimonials', 'skt-trust'),
		'desc' => __('[view_all_testimonials]', 'skt-trust'),
		'type' => 'info');
		
	$options[] = array(
		'name' => __('Contact Form', 'skt-trust'),
		'desc' => __('[contactform to_email="test@example.com" title="Contact Form"]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Latest Blog Post', 'skt-trust'),
		'desc' => __('[latestposts show="4"]', 'skt-trust'),
		'type' => 'info');		

	$options[] = array(
		'name' => __('Our Team', 'skt-trust'),
		'desc' => __('[ourteam show="4"]', 'skt-trust'),
		'type' => 'info');					

	$options[] = array(
		'name' => __('Footer posts', 'skt-trust'),
		'desc' => __('[footer-posts show="2"]', 'skt-trust'),
		'type' => 'info');	
		
	$options[] = array(
		'name' => __('Headings', 'skt-trust'),
		'desc' => __('[heading underline="yes" align="left"]Professional Wordpress Theme[/heading]', 'skt-trust'),
		'type' => 'info');					

	$options[] = array(
		'name' => __('Read More Button', 'skt-trust'),
		'desc' => __('[readmore-link align="left" button="Read More" links="#" target=""]', 'skt-trust'),
		'type' => 'info');	

	$options[] = array(
		'name' => __('Clear Class', 'skt-trust'),
		'desc' => __('[clear]', 'skt-trust'),
		'type' => 'info');	

	$options[] = array(
		'name' => __('Social Icons ( Note: More social icons can be found at: http://fortawesome.github.io/Font-Awesome/icons/)', 'skt-trust'),
		'desc' => __('[social_area]
    [social icon="facebook" link="#"]
    [social icon="twitter" link="#"]
    [social icon="linkedin" link="#"]
    [social icon="pinterest" link="#"]
    [social icon="google-plus" link="#"]
	[social icon="youtube" link="#"]
	[social icon="wordpress" link="#"]
	[social icon="flickr" link="#"]
	[social icon="skype" link="#"]
[/social_area]', 'skt-trust'),
		'type' => 'info');	
		
		
	$options[] = array(
			'name' => __('Full Width Column Content', 'skt-trust'),
			'desc' => __('<pre>
	[column_content type="full-width" class="" background=""]
		Column 1 Content goes here...
	[/column_content]
	</pre>', 'skt-trust'),
			'type' => 'info');	
	
	$options[] = array(
			'name' => __('2 Column Content', 'skt-trust'),
			'desc' => __('<pre>
	[column_content type="one_half" class="" background=""]
		Column 1 Content goes here...
	[/column_content]
	
	[column_content type="one_half_last" class="" background=""]
		Column 2 Content goes here...
	[/column_content]
	</pre>', 'skt-trust'),
			'type' => 'info');	
	
		$options[] = array(
			'name' => __('3 Column Content', 'skt-trust'),
			'desc' => __('<pre>
	[column_content type="one_third" class="" background=""]
		Column 1 Content goes here...
	[/column_content]
	
	[column_content type="one_third" class="" background=""]
		Column 2 Content goes here...
	[/column_content]
	
	[column_content type="one_third_last" class="" background=""]
		Column 3 Content goes here...
	[/column_content]
	</pre>', 'skt-trust'),
			'type' => 'info');	
	
		$options[] = array(
			'name' => __('4 Column Content', 'skt-trust'),
			'desc' => __('<pre>
	[column_content type="one_fourth" class="" background=""]
		Column 1 Content goes here...
	[/column_content]
	
	[column_content type="one_fourth" class="" background=""]
		Column 2 Content goes here...
	[/column_content]
	
	[column_content type="one_fourth" class="" background=""]
		Column 3 Content goes here...
	[/column_content]
	
	[column_content type="one_fourth_last" class="" background=""]
		Column 4 Content goes here...
	[/column_content]
	</pre>', 'skt-trust'),
			'type' => 'info');	
	
		$options[] = array(
			'name' => __('5 Column Content', 'skt-trust'),
			'desc' => __('<pre>
	[column_content type="one_fifth" class="" background=""]
		Column 1 Content goes here...
	[/column_content]
	
	[column_content type="one_fifth" class="" background=""]
		Column 2 Content goes here...
	[/column_content]
	
	[column_content type="one_fifth" class="" background=""]
		Column 3 Content goes here...
	[/column_content]
	
	[column_content type="one_fifth" class="" background=""]
		Column 4 Content goes here...
	[/column_content]
	
	[column_content type="one_fifth_last" class="" background=""]
		Column 5 Content goes here...
	[/column_content]
	</pre>', 'skt-trust'),
			'type' => 'info');	

	$options[] = array(
		'name' => __('Accordion', 'skt-trust'),
		'desc' => __('[accordion_section][accordion title="Accordion Title 1" content="Accordion Content Description"][/accordion][accordion title="Accordion Title 1" content="Accordion Content Description"][/accordion][accordion title="Accordion Title 1" content="Accordion Content Description"][/accordion][/accordion_section]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Skillbar', 'skt-trust'),
		'desc' => __('[skillbar_section][skillbar skillbar_bgcolor="#1e1e1e" title="Skill Bar Title" title_color="#000000" content="Skill Bar Content Area" content_color="#383939" percent="48.89%" percent_color="#ffffff" percent_bgcolor="#ffd800"][skillbar skillbar_bgcolor="#1e1e1e" title="Skill Bar Title" title_color="#000000" content="Skill Bar Content Area" content_color="#383939" percent="94%" percent_color="#ffffff" percent_bgcolor="#ffd800"][skillbar skillbar_bgcolor="#1e1e1e" title="Skill Bar Title" title_color="#000000" content="Skill Bar Content Area" content_color="#383939" percent="61.94%" percent_color="#ffffff" percent_bgcolor="#ffd800"][/skillbar_section]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Causes Code', 'skt-trust'),
		'desc' => __('[causes_main][our_causes image="add image url here" title="Causes title here" content="add content here for causes" skillbar_bgcolor="#eeeeee" percent="50%" percent_color="#000000" percent_bgcolor="#ffd800" raised_title="Raised" raised_price="$25.00" goal_title="Goal" goal_price="$50.00" donate_button="Donate Now" link="#" class="last"][/causes_main]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Why Choose Us', 'skt-trust'),
		'desc' => __('[choose_us_main][Choose_us icon="icon name here" image="" link="" title="title of choose" content="choose content"][/choose_us_main]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Event Code', 'skt-trust'),
		'desc' => __('[event_main][event image="add image url here" event_date="20" event_month="July" title="event title" event_time_icon="clock-o" event_time="08.00 - 16.00" event_location_icon="map-marker" event_location="Vallejo, California" content="event title" button="Read More" link="#" class="add class last it last"][/event][/event_main]', 'skt-trust'),
		'type' => 'info');

	$options[] = array(
		'name' => __('Client Logo', 'skt-trust'),
		'desc' => __('[client_logo_section][client_logo link="#" image="add client logo here"][/client_logo][/client_logo_section]', 'skt-trust'),
		'type' => 'info');

	// Support					
	$options[] = array(
		'name' => __('Our Themes', 'skt-trust'),
		'type' => 'heading');

	$options[] = array(
		'desc' => __('SKT Trust WordPress theme has been Designed and Created by <a href="'.esc_url('https://sktthemes.net/').'" target="_blank">SKT Themes</a>', 'skt-trust'),
		'type' => 'info');	

	 $options[] = array(
		'desc' => __('<a href="'.esc_url('https://sktthemes.net/').'" target="_blank"><img src="'.get_template_directory_uri().'/images/sktskill.jpg"></a>', 'skt-trust'),
		'type' => 'info');	

	return $options;
}