<?php
/**
Template name: Contact Us

 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package SKT Trust
 */

get_header(); ?>

  <div class="contact-page">
    <div class="content-area">
        <div class="site-main">
          <?php while(have_posts()) : the_post() ?>
          <?php the_content(); ?>
          <?php endwhile; ?>
        </div>
        <div class="contact_right">
          <?php if ( !dynamic_sidebar('sidebar-contact')) : ?>
          <aside class="widget">
          <?php if( of_get_option('contacttitle',true) != ''){ ?>
          <h3 class="widget-title"><?php echo of_get_option('contacttitle'); ?></h3>
          <?php } ?>
                  <p class="parastyle">
                    <?php if( of_get_option('address',true) != '') { echo of_get_option('address',true) ; } ; ?>
                    <?php if( of_get_option('address2',true) != '') { echo '<br />'; echo of_get_option('address2',true) ; } ; ?>
                  </p>
                  <div class="phone-no">
                    <p><?php if( of_get_option('phone',true) != ''){ ?><?php echo of_get_option('phone'); ?><?php } ?>
                    <span class="phno2"><?php if( of_get_option('phone2',true) != ''){ ?><?php echo of_get_option('phone2'); ?><?php } ?></span></p>
                    <p><?php if( of_get_option('email',true) != '' ) { ?><?php echo of_get_option('email',true) ; ?><?php } ?></p>
                    <?php if( of_get_option('weblink',true) != '' ) { ?><p><?php echo of_get_option('weblink',true) ; ?></p><?php } ?>
                  </div>
                  <div class="clear"></div>
              </aside>
          <?php endif; ?>
        </div><!-- .contact_right -->
        <div class="clear"></div>
      </div>
    
		<?php if(of_get_option('contactmap')) : ?>
        <div class="sitefull"> <?php echo do_shortcode(of_get_option('contactmap')); ?> </div>
        <?php endif; ?>
	</div>
    
<?php get_footer(); ?>
