<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package SKT Trust
 */
?>
<?php if( function_exists('is_woocommerce') && is_woocommerce() ) { ?>
<?php } ?>
<div id="sidebar" <?php if( is_page_template('blog-post-left-sidebar.php')){?> style="float:left;"<?php } ?>>
    
    <?php if ( ! dynamic_sidebar( 'sidebar-main' ) ) : ?>
     <h3 class="widget-title">Contact Form</h3>
        <aside id="archives" class="widget">           
            <?php echo do_shortcode('[contactform to_email="test@example.com" title="Contact Form"]'); ?>
        </aside>
    <?php endif; // end sidebar widget area ?>
	
</div><!-- sidebar -->

<?php if( function_exists('is_woocommerce') && is_woocommerce() ) { ?>
    <div class="clear"></div>
<?php } ?>