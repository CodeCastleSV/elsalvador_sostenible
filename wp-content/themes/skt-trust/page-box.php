<?php if ( is_home() || is_front_page() ) { ?>
	<?php if ( (of_get_option('pagesboxshowhide', true) != 'hide') ) { ?>
    
    <div id="wrapOne">
    <div class="container">
    	<?php if(of_get_option('sectionfrtitle')) : ?><h2 class="section_title"><?php echo of_get_option('sectionfrtitle', true); ?></h2><?php endif; ?>
    
        <div class="one_four_page-wrap">
          <?php
                $boxArr = array();
                   if( of_get_option('box1',true) != '' ){
                    $boxArr[] = of_get_option('box1',false);
                   }
                   if( of_get_option('box2',true) != '' ){
                    $boxArr[] = of_get_option('box2',false);
                   }
                   if( of_get_option('box3',true) != '' ){
                    $boxArr[] = of_get_option('box3',false);
                   }
                   if( of_get_option('box4',true) != '' ){
                    $boxArr[] = of_get_option('box4',false);
                   }
                   if( of_get_option('box5',true) != '' ){
                    $boxArr[] = of_get_option('box5',false);
                   }
                   if( of_get_option('box6',true) != '' ){
                    $boxArr[] = of_get_option('box6',false);
                   }
                
                if (!array_filter($boxArr)) {
                for($fx=1; $fx<=4; $fx++) {
                ?>
              <div class="one_four_page <?php if($fx % 4 == 0) { echo "last_column"; } ?>">
                    
                        <div class="one_four_page_thumb"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/features<?php echo $fx;?>.jpg" /></a></div>
                        <div class="one_four_page_content">
                        <h4>
						<?php if($fx == 1) {?>
                        <?php _e('Children To Get Shelter','skt-trust'); ?>
                        <?php } ?>

						<?php if($fx == 2) {?>
                        <?php _e('Girls To Get Education','skt-trust'); ?>
                        <?php } ?>

						<?php if($fx == 3) {?>
                        <?php _e('Send Vegetables To Africa','skt-trust'); ?>
                        <?php } ?>

						<?php if($fx == 4) {?>
                        <?php _e('Economic Opportunity','skt-trust'); ?>
                        <?php } ?>
                        </h4>
                        <p><?php _e('Nam tellus turpis blandit et ligula sit amet mattis congue semper euismod massa auctor magna eget nunc iaculis nunc.','skt-trust'); ?></p>
                        <a href="#" class="read-more">Donate Now</a>
                    </div>
                        
                
                </div>
              <!-- one_four_page -->
          <?php 
                } 
                } else {
                    $no = 0;
                    $fx = 1;
				$box_column = array('no_column','one_column','two_column','three_column','four_column','five_column','six_column');
                    $queryvar = new wp_query(array('post_type' => 'page', 'post__in' => $boxArr, 'posts_per_page' => 6, 'orderby' => 'post__in' ));
                    while( $queryvar->have_posts() ) : $queryvar->the_post(); $no++; ?>
          <?php if($box_column[count($boxArr)]) { ?>
          <div class="one_four_page <?php echo $box_column[count($boxArr)]; ?> <?php if($fx % count($boxArr) == 0) { echo "last_column"; } ?>">
			  <?php $boximage = of_get_option('boximage'.$fx, true)?>
              <?php if(! empty($boximage)) : ?>
              <div class="one_four_page_thumb"><a href="<?php the_permalink(); ?>"><img src="<?php echo of_get_option('boximage'.$fx, true)?>" alt=""></a></div>
              <?php endif; ?>
                <div class="one_four_page_content">
                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
                <p><?php echo wp_trim_words(get_the_content(), of_get_option('topboxescontentlength'))?></p>
                <a href="<?php the_permalink(''); ?>" class="read-more"><?php echo of_get_option('readmorebtntexttpbx'); ?></a>
                </div>
            </div>
          <?php } ?>
          <?php $fx++; endwhile; wp_reset_query(); ?>
          <?php }?>
        </div><!-- .one_four_page-wrap-->
        
        <div class="clear"></div>
        </div>
    </div><!-- #wrapOne -->
    <?php } ?>
	<div class="clear"></div>
<?php } ?>
