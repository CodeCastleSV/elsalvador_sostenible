<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta name="viewport" content="width=device-width">
<title>SKT Trust &#8211; Professional WordPress Theme</title>
<style>.header-top {background:#ffd800;}.header-top, .header-top a {color:#282828;}.header-top .social-icons a, .header-top .social-icons {color:#282828; border-color:#282828;}.header-top .social-icons a:hover {background:#282828;}.header .header-inner{background:#26211d;}body{font-family:Lato; font-size:13px; color:#000000; font-weight:400; text-transform:none; font-style:normal; background:#ffffff;}.toggle a{background-color:#ffd800;}.content-area .phone-no a, .admin-post a, .our-testimonials .testimonial-content{color:#000000;}.header .header-inner .logo h2{font-family:Lato; font-size:35px; color:#ffffff; font-weight:300; text-transform:inherit; font-style:normal;}.header .header-inner .logo a span.tagline{font-family:Lato; font-size:14px; color:#ffffff; font-weight:400; text-transform:none; font-style:normal;}.header .header-inner .logo img{height:35px;}.header .header-inner .nav ul li a {font-family:Lato; font-size:15px; color:#a19389; font-weight:700; text-transform:none; font-style:normal;}.header .header-inner .nav ul li ul li a, .header .header-inner .nav ul li.current_page_item ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a {color:#a19389; border-color:#848484;}.header .header-inner .nav ul li a:hover, .header .header-inner .nav ul li.current_page_item a, .header .header-inner .nav ul li.current_page_item ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a:hover, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.current_page_item a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li.current_page_item a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children.current_page_ancestor a, .header .header-inner .nav ul li.current_page_ancestor a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_item a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li.current_page_item a {background-color: #ffd800; color:#282828;}.header .header-inner .nav ul li ul li a, .header .header-inner .nav ul li.current_page_item ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.menu-item-has-children:hover ul li.menu-item-has-children:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li a, .header .header-inner .nav ul li.current_page_ancestor:hover ul li.current_page_ancestor:hover ul li a {background-color: #26211d;}@media screen and (max-width:1023px){.header .header-inner .nav ul li a:hover, .header .header-inner .nav ul li.current_page_ancestor a:hover {background-color: #ffd800 !important; color:#282828 !important;}}@media screen and (max-width:1023px){.header .header-inner .nav ul li a {color:#a19389 !important;}}.slide_info h2{font-family:Lato, cursive; font-size:40px; color:#ffffff; font-weight:700; text-transform:inherit; font-style:italic;}.slide_info {font-family:Lato; font-size:13px; color:#ffffff; font-weight:400; text-transform:inherit; font-style:normal;}.slide_info h3 {color:#ffffff !important;}a.sldbutton{ border-color:#ffffff;  color:#ffffff !important; }a.sldbutton:hover{ background-color:#ffd800; color:#ffffff !important; border-color:#ffd800;}.nivo-directionNav a{background-color:#ffffff;}.nivo-directionNav a:hover{background-color:#ffd800;}.nivo-controlNav a, ol.nav-numbers li a, .owl-theme .owl-dots .owl-dot span{background-color:#e7e7e7;}.nivo-controlNav a.active, ol.nav-numbers li.active a, .owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span{background-color:#ffd800;}.choose-thumb {border-color:#ffd800;}.choose-col i, .choose-col {color:#222222;}.choose-col:hover i{color:#ffd800;}.event-content {color:#949393;}.event-thumb h3{color:#383939; background-color:#ffd800;}.causes-content, .cuase-raised span, .cuase-goal span  {color:#858484;}.causes-col .skill-bar-percent::after {border-top-color:#ffd800;}.our-testimonials i{color:#ffd800;}.client-logo {margin:0 2px 0 0; background:#f7f7f7; border-color:#f7f7f7;}.client-logo:hover {border-color:#ffd800;}h2.section_title, .professional-content h2 {font-family:Lato; font-size:25px; color:#2b2b2b; font-weight:700; text-transform:inherit; font-style:normal;}#testimonial-section .testimonial-content, #testimonial-section .testimonial-content h5, section .testimonial-content, section .testimonial-content h5 {color:#f6f4f4;}.controls li {color:#ffffff; background:#ffd800;}.controls li.active, .controls li:hover {background:#ffd800; color:#282828;}#Grid .mix {background-color:#ffd800;}a, .footer a:hover, .recent-post:hover h5 , .copyright a:hover, .footer .cols-1 .widget-column-1 ul li a:hover, .cols-1 .widget-column-1 ul li.current_page_item a, .footer ul li.current_page_item, .footer ul li:hover a, .footer ul li:hover:before, .footer .phone-no a:hover, .woocommerce table.shop_table th, .woocommerce-page table.shop_table th, .post-cate, .news-box h2:hover, .news-posts-share:hover, .servies-col i, .professional-content li a:hover, .one_four_page:hover h4, .admin-post a:hover, .choose-col:hover i  {color:#ffd800;}a:hover, .professional-content li a{color:#5b5a5a;}.social-icons a{color:#ffffff; background:#545556;}.social-icons a:hover {background:#ffd800; color:#ffffff;}.view-all-btn a, #contactform_main input[type='submit']:hover, .wpcf7 form input[type='submit'], input[type='submit'], .button:hover, #commentform input#submit, input.search-submit, .post-password-form input[type=submit], .read-more, .pagination .current, .pagination a:hover, .compatible .read-more, .accordion-section .ui-accordion .ui-accordion-header span.fa, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, a.added_to_cart:hover, .causes-col .causes-thumb a.read-more:hover {background-color:#383939; color:#ffffff;}.view-all-btn a:hover, #contactform_main input[type='submit']:hover, .wpcf7 form input[type='submit']:hover, input[type='submit']:hover, .button:hover, #commentform input#submit:hover, input.search-submit:hover, .post-password-form input[type=submit]:hover, .read-more:hover , .accordion-box h2:before, .pagination span, .pagination a, .luxury-experience a:hover, a.added_to_cart, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .slider-form  h3:after, .compatible .read-more:hover, .one_four_page:hover .read-more, .accordion-section .ui-accordion .ui-accordion-header.ui-accordion-header-active span.fa, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .causes-col .causes-thumb a.read-more {background-color:#ffd800; color:#383939;}#footer-wrapper{background-color:#26211d; border-color:#ffd800;}#footer-wrapper, .footer .recent-post h6 {color:#ffffff;}.copyright{background-color:#1b1715; color:#ffffff;}.footer h5{font-family:Lato; font-size:15px; color:#ffd800; font-weight:700; text-transform:uppercase; font-style:normal;}h3.widget-title, .searchbox-icon, #sidebar .woocommerce-product-search input[type='submit']{background-color:#ffd800 ; color:#ffffff;}h3.widget-title, .contact-page .site-main h3{ color:#ffffff;}aside.widget, .comment-form-comment textarea, .our-testimonials, .tm_description {background-color:#f9f9f9 !important;}.tm_description::after {border-top-color:#f9f9f9 !important;}#sidebar ul li, #sidebar .home-sidebar ul li ul li{border-color:#000000;}#sidebar ul li a, .category-post li a, .searchbox-input, .searchbox-input::-moz-placeholder {color:#000000;}.searchbox-input{background-color:#f9f9f9;}#contactform_main input[type='text'], #contactform_main input[type='email'], #contactform_main input[type='tel'], #contactform_main input[type='url'], #contactform_main textarea, .wpcf7 form input[type='text'], .wpcf7 form input[type='email'], .wpcf7 form input[type='tel'], .wpcf7 form textarea, select, input.appointfield[type='text'], #sidebar select, input[type='text'], input[type='email'], input[type='tel'], textarea {background-color:#ffffff; color:#2b2b2b; border-color:#e7e7e7;}.accordion-section .ui-accordion .ui-accordion-header, .accordion-section .ui-accordion .ui-accordion-content {border-color:#e7e7e7;}h1{font-family:Lato; font-size:30px; color:#282828; font-weight:400; text-transform:uppercase; font-style:normal;}h2{font-family:Lato; font-size:25px; color:#2b2b2b; font-weight:700; text-transform:inherit; font-style:normal;}h3{font-family:Lato; font-size:20px; color:#333333; font-weight:700; text-transform:uppercase; font-style:normal;}h4{font-family:Lato; font-size:17px; color:#333333; font-weight:700; text-transform:uppercase; font-style:normal;}h5{font-family:Lato; font-size:15px; color:#000000; font-weight:700; text-transform:uppercase; font-style:normal;}h6{font-family:Lato; font-size:14px; color:#000000; font-weight:400; text-transform:inherit; font-style:normal;}#sidebar .price_slider_wrapper .ui-widget-content{background-color:#cccccc;}#sidebar .ui-slider .ui-slider-handle{background-color:#ffd800;}#sidebar .ui-slider .ui-slider-range{background-color:#515151;}.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}.ourteam_col{width:267px;}</style>
</head>

<body class="home blog logged-in">
<div id="main">
  <div class="header">
    <div class="header-top">
      <div class="container">
        <div class="header-top-left">
          <ul>
            <li><a href="#"><i class="fa fa-phone"></i> 12 8888 66666</a></li>
            <li><a href="mailto:info@sitename.com"><i class="fa fa-envelope"></i> info@sitename.com</a></li>
          </ul>
        </div>
        <div class="header-top-right">
          <div class="social-icons"><a href="#" target="_blank" class="fa fa-facebook" title="facebook"></a><a href="#" target="_blank" class="fa fa-twitter" title="twitter"></a><a href="#" target="_blank" class="fa fa-linkedin" title="linkedin"></a><a href="#" target="_blank" class="fa fa-google-plus" title="google-plus"></a><a href="#" target="_blank" class="fa fa-youtube" title="youtube"></a></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <!-- Header Top -->
    
    <div class="header-inner">
      <div class="container">
        <div class="logo">
        <a href="#"><h2>SKT Trust</h2><span class="tagline">Professional Wordpress Theme</span> </a>
        </div>
        <!-- logo -->
        
        <div class="toggle"> <a class="toggleMenu" href="#">Menu</a></div>
        <!-- toggle -->
        <div class="nav">
          <div class="menu-primary-container">
            <ul id="menu-primary" class="menu">
              <li class="current_page_item"><a href="#">Home</a></li>
              <li><a href="#">Blog</a></li>
              <li><a href="#">Pages</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <!-- nav -->
        <div class="clear"></div>
      </div>
      <!-- Container --> 
    </div>
    <!-- header-inner -->
    
    <div class="slider-main">
      <div id="slider" class="nivoSlider"><img src="<?php echo get_template_directory_uri(); ?>/images/slides/slider1.jpg" alt="" title="#slidecaption1"/> </div>
      <div id="slidecaption1" class="nivo-html-caption">
        <div class="slide_info">
          <h2>Donate & Help</h2>
          <div class="slide-row">Donec id laoreet orci. Quisque lacus nulla, dapibus sed nisi vel, faucibus sodales odio. Donec eget euismod mi. Phasellus malesuada tristique eros ac volutpat. Aenean venenatis, risus id fermentum facilisis, purus metus mattis tortor, quis convallis velit lorem sit amet sapien</div>
          <a class="sldbutton" href="#"> Buy This Theme </a> </div>
        <!-- slide_info --> 
      </div>
      <div class="clear"></div>
    </div>
    <!-- slider --> 
    
  </div>
  <!-- header -->
  
  <div id="wrapOne">
    <div class="container">
      <h2 class="section_title"><span>Help people by donating little.</span> See our causes</h2>
      <div class="one_four_page-wrap">
        <div class="one_four_page ">
          <div class="one_four_page_thumb"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/features1.jpg" /></a></div>
          <div class="one_four_page_content">
            <h4> Children To Get Shelter </h4>
            <p>Nam tellus turpis blandit et ligula sit amet mattis congue semper euismod massa auctor magna eget nunc iaculis nunc.</p>
            <a href="#" class="read-more">Donate Now</a> </div>
        </div>
        <!-- one_four_page -->
        <div class="one_four_page ">
          <div class="one_four_page_thumb"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/features2.jpg" /></a></div>
          <div class="one_four_page_content">
            <h4> Girls To Get Education </h4>
            <p>Nam tellus turpis blandit et ligula sit amet mattis congue semper euismod massa auctor magna eget nunc iaculis nunc.</p>
            <a href="#" class="read-more">Donate Now</a> </div>
        </div>
        <!-- one_four_page -->
        <div class="one_four_page ">
          <div class="one_four_page_thumb"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/features3.jpg" /></a></div>
          <div class="one_four_page_content">
            <h4> Send Vegetables To Africa </h4>
            <p>Nam tellus turpis blandit et ligula sit amet mattis congue semper euismod massa auctor magna eget nunc iaculis nunc.</p>
            <a href="#" class="read-more">Donate Now</a> </div>
        </div>
        <!-- one_four_page -->
        <div class="one_four_page last_column">
          <div class="one_four_page_thumb"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/features4.jpg" /></a></div>
          <div class="one_four_page_content">
            <h4> Economic Opportunity </h4>
            <p>Nam tellus turpis blandit et ligula sit amet mattis congue semper euismod massa auctor magna eget nunc iaculis nunc.</p>
            <a href="#" class="read-more">Donate Now</a> </div>
        </div>
        <!-- one_four_page --> 
      </div>
      <!-- .one_four_page-wrap-->
      
      <div class="clear"></div>
    </div>
  </div>

<section>
  <div class="container">
     <h4> Desarrollo</h4>
    <?php $recent = new WP_Query(<"page_id=233";);
      while($recent->have_posts()) : $recent->the_post();?>
       <h3><?php the_title(); ?></h3>
       <?php the_content(); ?>
    <?php endwhile; ?>
  </div>
</section>

  <!-- #wrapOne -->
  <div class="clear"></div>
  <section style="background-color:#f7f7f7; " id="" class="">
  <div class="container" >
    <div class="">
      <div class="cloumn_row">
        <div class="one_half" class="">
        <h2>How Can Help You?</h2>
        <p>
        <div class="accordion-section">
          <div id="accordion">
            <h6>Media</h6>
            <div>
              <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam.</p>
            </div>
            <h6>Becone Volunteer</h6>
            <div>
              <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam.</p>
            </div>
            <h6>Send Donation</h6>
            <div>
              <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus hendrerit quis ante eget, lobortis elemtum neque. Aliquam in ullamcorper quam.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="one_half last_column">
      <br />
      <h2>Donation Count</h2>
      <p>
      <div class="skillbar-section">
        <div class="skillbar-title" style="color:#000000;">Children to get Shelter</div>
        <div class="skillbar " data-percent="61.94%" style="background:#1e1e1e;">
          <div class="skillbar-bar" style="color:#383939; background:#ffd800;"><span>$78,354.00/ $1,26,500.00</span></div>
          <div class="skill-bar-percent" style="color:#ffffff;">61.94%</div>
        </div>
        <div class="skillbar-title" style="color:#000000;">Girls to get Education</div>
        <div class="skillbar " data-percent="73.64%" style="background:#1e1e1e;">
          <div class="skillbar-bar" style="color:#383939; background:#ffd800;"><span>$78,354.00/ $1,26,500.00</span></div>
          <div class="skill-bar-percent" style="color:#ffffff;">73.64%</div>
        </div>
        <div class="skillbar-title" style="color:#000000;">Send Vegetables to Africa</div>
        <div class="skillbar " data-percent="86.58%" style="background:#1e1e1e;">
          <div class="skillbar-bar" style="color:#383939; background:#ffd800;"><span>$78,354.00/ $1,26,500.00</span></div>
          <div class="skill-bar-percent" style="color:#ffffff;">86.58%</div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>
<!-- middle-align -->
</div>
<!-- container -->
</section>
<div class="clear"></div>
<section  id="" class="">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Our Causes</h2>
      <div class="causes-section">
        <div class="causes-col ">
          <div class="causes-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/causes-thumb1.jpg" alt="" /><a href="#" class="read-more">Donate Now</a></div>
          <div class="causes-content">
            <h5>charity activity of the year</h5>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod.</p>
            <div class="skillbar " data-percent="12%" style="background:#eeeeee;">
              <div class="skillbar-bar" style="color:; background:#ffd800;">
                <div class="skill-bar-percent" style="color:#000000; background:#ffd800;">12%</div>
              </div>
            </div>
            <div class="cuase-raised">
              <h5><span>Raised</span>$12</h5>
            </div>
            <div class="cuase-goal">
              <h5><span>Goal</span>$50.00</h5>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="causes-col ">
          <div class="causes-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/causes-thumb2.jpg" alt="" /><a href="#" class="read-more">Donate Now</a></div>
          <div class="causes-content">
            <h5>food and shelter for orphans</h5>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod.</p>
            <div class="skillbar " data-percent="90%" style="background:#eeeeee;">
              <div class="skillbar-bar" style="color:; background:#ffd800;">
                <div class="skill-bar-percent" style="color:#000000; background:#ffd800;">90%</div>
              </div>
            </div>
            <div class="cuase-raised">
              <h5><span>Raised</span>$40.00</h5>
            </div>
            <div class="cuase-goal">
              <h5><span>Goal</span>$50.00</h5>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="causes-col last">
          <div class="causes-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/causes-thumb3.jpg" alt="" /><a href="#" class="read-more">Donate Now</a></div>
          <div class="causes-content">
            <h5>education in africa</h5>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod.</p>
            <div class="skillbar " data-percent="50%" style="background:#eeeeee;">
              <div class="skillbar-bar" style="color:; background:#ffd800;">
                <div class="skill-bar-percent" style="color:#000000; background:#ffd800;">50%</div>
              </div>
            </div>
            <div class="cuase-raised">
              <h5><span>Raised</span>$25.00</h5>
            </div>
            <div class="cuase-goal">
              <h5><span>Goal</span>$50.00</h5>
            </div>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section style="background-color:#f7f7f7; " id="" class="">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Why Choose Us</h2>
      <div class="choose_us_main">
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-paper-plane fa-4x"></i></div>
          <div class="choose-content">
            <h5>Guareanteed Results</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-user fa-4x"></i></div>
          <div class="choose-content">
            <h5>Your Charitable Life</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-tachometer fa-4x"></i></div>
          <div class="choose-content">
            <h5>No Goal Reuirements</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-heart fa-4x"></i></div>
          <div class="choose-content">
            <h5>Most Trusted</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-credit-card-alt fa-4x"></i></div>
          <div class="choose-content">
            <h5>Maximize Tax Advantages</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="choose-col">
          <div class="choose-thumb"><i class="fa fa-star fa-4x"></i></div>
          <div class="choose-content">
            <h5>Our Experience</h5>
            <p>Vivamus quis enim egestas, lobortis nunc at, euismod ex. Donec non condimentum urna. Cras sed mattis lorem.</p>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section  id="" class="">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Upcoming Events</h2>
      <div class="event_main">
        <div class="event-col ">
          <div class="event-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/event-thumb1.jpg" alt="">
            <h3><span>20</span>July</h3>
          </div>
          <div class="event-content">
            <h3>Charity Marathon: Run for better life</h3>
            <div class="event-time"><i class="fa fa-clock-o"></i> 08.00 - 16.00</div>
            <div class="event-location"><i class="fa fa-map-marker"></i> Vallejo, California</div>
            <div class="clear"></div>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="event-col ">
          <div class="event-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/event-thumb2.jpg" alt="">
            <h3><span>20</span>July</h3>
          </div>
          <div class="event-content">
            <h3>Charity Marathon: Run for better life</h3>
            <div class="event-time"><i class="fa fa-clock-o"></i> 08.00 - 16.00</div>
            <div class="event-location"><i class="fa fa-map-marker"></i> Vallejo, California</div>
            <div class="clear"></div>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="event-col last">
          <div class="event-thumb"><img src="<?php echo get_template_directory_uri(); ?>/images/event-thumb3.jpg" alt="">
            <h3><span>20</span>July</h3>
          </div>
          <div class="event-content">
            <h3>Charity Marathon: Run for better life</h3>
            <div class="event-time"><i class="fa fa-clock-o"></i> 08.00 - 16.00</div>
            <div class="event-location"><i class="fa fa-map-marker"></i> Vallejo, California</div>
            <div class="clear"></div>
            <p>Sed suscipit mauris nec mauris vulputa apouere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula. Nam metus  hendrerit quis ante eget, lobortis elemtum</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section style="background-color:#f7f7f7; " id="" class="">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Our Team</h2>
      <div class="section-teammember">
        <div class="ourteam_col ">
          <div class="ourteam_thumb"><a href="#" title="Jane Doe"><img src="<?php echo get_template_directory_uri(); ?>/images/ourteam-1.jpg" alt=" " /></a></div>
          <div class="ourteam_content"><a href="#">
            <h5>John Doe</h5>
            </a>
            <h6>HR</h6>
            <p>Mauris fringilla, metus non sollicitudin condimentum, leo diam convallis massa, eu efficitur sem dolor malesuada neque. Sed aliquet, nisl vel iaculis feugiat,&hellip;</p>
          </div>
          <div class="social-icons"><a href="#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a><a href="#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" title="google-plus" target="_blank"><i class="fa fa-google-plus"></i></a><a href="#" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
            <div class="clear"></div>
          </div>
        </div>
        <div class="ourteam_col ">
          <div class="ourteam_thumb"><a href="#" title="Martin Doe"><img src="<?php echo get_template_directory_uri(); ?>/images/ourteam-2.jpg" alt=" " /></a></div>
          <div class="ourteam_content"><a href="#">
            <h5>Martin Doe</h5>
            </a>
            <h6>Programmer</h6>
            <p>In eu fringilla nisl. Curabitur dapibus suscipit semper. Vivamus vel scelerisque tellus. Nam convallis imperdiet risus, ac consequat urna gravida blandit. Cum&hellip;</p>
          </div>
          <div class="social-icons"><a href="#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a><a href="#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a><a href="#" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
            <div class="clear"></div>
          </div>
        </div>
        <div class="ourteam_col ">
          <div class="ourteam_thumb"><a href="#" title="John Doe"><img src="<?php echo get_template_directory_uri(); ?>/images/ourteam-3.jpg" alt=" " /></a></div>
          <div class="ourteam_content"><a href="#">
            <h5>Jane Doe</h5>
            </a>
            <h6>MD, EENT</h6>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis non velit quis lobortis. Pellentesque elementum tortor tempus velit euismod luctus. In&hellip;</p>
          </div>
          <div class="social-icons"><a href="#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a><a href="#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" title="google-plus" target="_blank"><i class="fa fa-google-plus"></i></a><a href="#" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
            <div class="clear"></div>
          </div>
        </div>
        <div class="ourteam_col  last">
          <div class="ourteam_thumb"><a href="#" title="Martina Doe"><img src="<?php echo get_template_directory_uri(); ?>/images/ourteam-4.jpg" alt=" " /></a></div>
          <div class="ourteam_content"><a href="#">
            <h5>David Doe</h5>
            </a>
            <h6>Marketing Lead</h6>
            <p>In eu fringilla nisl. Curabitur dapibus suscipit semper. Vivamus vel scelerisque tellus. Nam convallis imperdiet risus, ac consequat urna gravida blandit. Cum&hellip;</p>
          </div>
          <div class="social-icons"><a href="#" title="facebook" target="_blank"><i class="fa fa-facebook"></i></a><a href="#" title="twitter" target="_blank"><i class="fa fa-twitter"></i></a><a href="#" title="instagram" target="_blank"><i class="fa fa-instagram"></i></a><a href="#" title="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
            <div class="clear"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section  id="" class="">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Latest News</h2>
      <div class="latestnews">
        <div class="news-box">
          <div class="news-thumb"> <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/post-thumb1.jpg" alt="" /></a> </div>
          <div class="news"> <a href="#">
            <h5>Hendrerit cursus luctus</h5>
            </a> <span class="admin-post"><i class="fa fa-user"></i> By <a href="#">admin</a></span> <span class="post-date"><i class="fa fa-calendar"></i> on 19 July 2016</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ullamcorper tellus faucibus leo vestibulum, facilisis placerat lacus ultrices. Vivamus diam arcu, finibus vel magna ac, bibendum varius nibh. Vivamus varius...</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="news-box">
          <div class="news-thumb"> <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/post-thumb2.jpg" alt="" /></a> </div>
          <div class="news"> <a href="#">
            <h5>Quisque volutpat augue</h5>
            </a> <span class="admin-post"><i class="fa fa-user"></i> By <a href="#">admin</a></span> <span class="post-date"><i class="fa fa-calendar"></i> on 19 July 2016</span>
            <p>Nullam nunc risus, hendrerit cursus luctus sed, hendrerit et nulla. Nulla luctus augue ut gravida pulvinar. Nullam sit amet hendrerit dui. Fusce viverra felis in elementum pulvinar. Curabitur lobortis semper...</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="news-box">
          <div class="news-thumb"> <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/post-thumb3.jpg" alt="" /></a> </div>
          <div class="news"> <a href="#">
            <h5>Donec sagittis malesuada</h5>
            </a> <span class="admin-post"><i class="fa fa-user"></i> By <a href="#">admin</a></span> <span class="post-date"><i class="fa fa-calendar"></i> on 19 July 2016</span>
            <p>Suspendisse eu vulputate lorem. Donec sit amet lobortis sapien, in sagittis ipsum. Nam ipsum purus, maximus quis malesuada vitae, vestibulum id justo. Donec sagittis malesuada lorem, sit amet accumsan tortor...</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="news-box last">
          <div class="news-thumb"> <a href="#"><img src="<?php echo get_template_directory_uri();?>/images/post-thumb4.jpg" alt="" /></a> </div>
          <div class="news"> <a href="#">
            <h5>consectetur adipiscing elit</h5>
            </a> <span class="admin-post"><i class="fa fa-user"></i> By <a href="#">admin</a></span> <span class="post-date"><i class="fa fa-calendar"></i> on 19 July 2016</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer ullamcorper tellus faucibus leo vestibulum, facilisis placerat lacus ultrices. Vivamus diam arcu, finibus vel magna ac, bibendum varius nibh. Vivamus varius...</p>
            <a href="#" class="read-more">Read More</a> </div>
        </div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/testimonial-bg.jpg); background-repeat:no-repeat; background-position: center center; background-size: cover; " id="testimonial-section" class="menu_page">
  <div class="container" >
    <div class="">
      <p>
      <div class="our-testimonials">
        <div class="owl-carousel">
          <div class="testimonial-item">
            <div class="testimonial-content"> <i class="fa fa-quote-left fa-2x"></i>
              <p> Phasellus id ultrices diam. Morbi eleifend enim a libero porttitor elementum. Suspendisse tempor pharetra vulputate. Nam mattis pretium varius. Nullam nec erat tempus, hendrerit massa quis, scelerisque libero. Vivamus gravida, justo vitae pharetra tempus, sem enim pellentesque eros, sed porta diam velit et nulla. Integer erat orci, dictum ut velit sit amet, scelerisque facilisis neque. Sed nec congue purus. Donec ipsum felis, interdum vitae purus vitae, venenatis porttitor justo.</p>
              <div class="testimonial-thumb"><img src="<?php echo get_template_directory_uri();?>/images/testimonial-1.jpg" /></div>
              <h5>Jopesh Doe</h5>
              <div class="testimonial-detail">(Marketing, company)</div>
            </div>
          </div>
          <div class="testimonial-item">
            <div class="testimonial-content"> <i class="fa fa-quote-left fa-2x"></i>
              <p> Phasellus id ultrices diam. Morbi eleifend enim a libero porttitor elementum. Suspendisse tempor pharetra vulputate. Nam mattis pretium varius. Nullam nec erat tempus, hendrerit massa quis, scelerisque libero. Vivamus gravida, justo vitae pharetra tempus, sem enim pellentesque eros, sed porta diam velit et nulla. Integer erat orci, dictum ut velit sit amet, scelerisque facilisis neque. Sed nec congue purus. Donec ipsum felis, interdum vitae purus vitae, venenatis porttitor justo.</p>
              <div class="testimonial-thumb"><img src="<?php echo get_template_directory_uri();?>/images/testimonial-2.jpg" /></div>
              <h5>John Doe</h5>
              <div class="testimonial-detail">(Developer, company)</div>
            </div>
          </div>
        </div>
      </div>
      <div class="view-all-btn" style="text-align:center;"><a href="#" target="">View All Our Team</a></div>
      </p>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<section  id="client-section" class="menu_page">
  <div class="container" >
    <div class="">
      <h2 class="section_title">Our Partners</h2>
      <p>
      <div class="client_logo_section">
        <div class="client-logo "><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/client-logo1.jpg" alt=""></a></div>
        <div class="client-logo "><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/client-logo2.jpg" alt=""></a></div>
        <div class="client-logo "><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/client-logo3.jpg" alt=""></a></div>
        <div class="client-logo "><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/client-logo4.jpg" alt=""></a></div>
        <div class="client-logo last"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/client-logo5.jpg" alt=""></a></div>
        <div class="clear"></div>
      </div>
      <div class="view-all-btn" style="text-align:center;"><a href="#" target="">View All</a></div>
      </p>
      <div class="clear"></div>
    </div>
    <!-- middle-align --> 
  </div>
  <!-- container --> 
</section>
<div class="clear"></div>
<div id="footer-wrapper">
  <div class="container">
    <div class="footer">
      <div class="cols-4">
        <div class="widget-column-1">
          <h5>About Us</h5>
          <p>Sed suscipit mauris nec mauris vulputate, a posuere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod. Proin venenatis orci sit amet nisl finibus vehicula.</p>
          <p>Nam metus lorem, hendrerit quis ante eget, lobortis elementum neque. Aliquam in ullamcorper quam. Integer euismod ligula in mauris vehicula imperdiet. Cras in convallis ipsum. Phasellus turpis, aliquet non quis, tristique tempus turpis.</p>
        </div>
        <div class="widget-column-2">
          <h5>Latest Posts</h5>
          <div class="recent-post  "> <a href="#">
            <h6>Hendrerit cursus luctus</h6>
            </a>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing&hellip;</p>
            <div class="clear"></div>
          </div>
          <div class="recent-post  "> <a href="#">
            <h6>Quisque volutpat augue</h6>
            </a>
            <p>Nullam nunc risus, hendrerit cursus luctus sed,&hellip;</p>
            <div class="clear"></div>
          </div>
          <div class="recent-post last"> <a href="#">
            <h6>Donec sagittis malesuada</h6>
            </a>
            <p>Suspendisse eu vulputate lorem. Donec sit amet&hellip;</p>
            <div class="clear"></div>
          </div>
        </div>
        <div class="widget-column-3">
          <h5>Latest Twitts</h5>
          <a class="twitter-timeline" data-chrome="nofooter noheader noborders noscroll noscrollbar transparent" data-tweet-limit="1" data-link-color="#ffd800"  data-theme="dark" data-dnt="true" href="https://twitter.com/sktthemes"  data-widget-id="353086898853531648">Tweets by @sktthemes</a> </div>
        <div class="widget-column-4">
          <h5> Contact Info </h5>
          <p class="parastyle"> 123 Bridge Street,<br />
            New York, NY 666555 </p>
          <div class="phone-no">
            <p><span class="phno">Phone: 1.800.555.6789</span> <span class="phno2"></span></p>
            <p>Email: <a href="mailto:support@sitename.com">support@sitename.com</a></p>
            <p>website: <a href="https://sktthemes.net/" target="_blank">sktthemes.net</a></p>
          </div>
          <div class="social-icons"> <a href="#" target="_blank" class="fa fa-facebook" title="facebook"></a> <a href="#" target="_blank" class="fa fa-google-plus" title="google-plus"></a> <a href="#" target="_blank" class="fa fa-twitter" title="twitter"></a> <a href="#" target="_blank" class="fa fa-linkedin" title="linkedin"></a> <a href="#" target="_blank" class="fa fa-pinterest" title="pinterest"></a> <a href="#" target="_blank" class="fa fa-youtube" title="youtube"></a> </div>
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      <!--end .cols-4--> 
    </div>
    <!--end .footer--> 
    
  </div>
  <!--end .container-->
  
  <div class="copyright">
    <div class="container">
      <div class="copyright-txt"> Copyright 2016 <a href="https://www.sktthemes.net/" target="_blank">SKT Themes</a> All Rights Reserved </div>
      <div class="design-by"> </div>
      <div class="clear"></div>
    </div>
    <!-- copyright --> 
  </div>
</div>
<!-- footer-wrapper -->
</body>
</html>